package Entity;

import java.util.ArrayList;

public class StudentExam {
	
	private int id;
	private int userid;
	private int exampaperid;
	private String starttime;
	private String endtime;
	private String strquestionslist;
	private int score;
	private int status;
	private ArrayList<ExamQuestion> examquestionlist;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getExampaperid() {
		return exampaperid;
	}
	public void setExampaperid(int exampaperid) {
		this.exampaperid = exampaperid;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getStrquestionslist() {
		return strquestionslist;
	}
	public void setStrquestionslist(String strquestionslist) {
		this.strquestionslist = strquestionslist;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public ArrayList<ExamQuestion> getExamquestionlist() {
		return examquestionlist;
	}
	public void setExamquestionlist(ArrayList<ExamQuestion> examquestionlist) {
		this.examquestionlist = examquestionlist;
	}
	
	
	

}
