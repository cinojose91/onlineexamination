package Entity;

import java.util.ArrayList;

public class ExamPaper {
	private int id;
	private String name;
	private String description;
	private int year;
	private int semester;
	private int examhours;
	private int exammins;
	private int status;
	private ArrayList<ExamQuestion> eqlist;
	private int noofquestions;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getSemester() {
		return semester;
	}
	public void setSemester(int semester) {
		this.semester = semester;
	}
	public ArrayList<ExamQuestion> getEqlist() {
		return eqlist;
	}
	public void setEqlist(ArrayList<ExamQuestion> eqlist) {
		this.eqlist = eqlist;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getExamhours() {
		return examhours;
	}
	public void setExamhours(int examhours) {
		this.examhours = examhours;
	}
	public int getExammins() {
		return exammins;
	}
	public void setExammins(int exammins) {
		this.exammins = exammins;
	}
	public int getNoofquestions() {
		return noofquestions;
	}
	public void setNoofquestions(int noofquestions) {
		this.noofquestions = noofquestions;
	}
	

}
