package Entity;

import java.util.ArrayList;

public class ExamQuestion {
	private int id;
	private String question;
	private int score;
	private int exampaperid;
	private int status;

	private ArrayList<ExamAnswer> examAnswerList;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestion() {
		if(question==null){
			question="";
		}
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getExampaperid() {
		return exampaperid;
	}
	public void setExampaperid(int exampaperid) {
		this.exampaperid = exampaperid;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public ArrayList<ExamAnswer> getExamAnswerList() {
		return examAnswerList;
	}
	public void setExamAnswerList(ArrayList<ExamAnswer> examAnswerList) {
		this.examAnswerList = examAnswerList;
	}
	
	
	
	
	
	

}
