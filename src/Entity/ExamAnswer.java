package Entity;

public class ExamAnswer {
	private int id;
	private String answer;
	private int isCorrect;
	private int examquestionid;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAnswer() {
		if(answer==null){
			answer="";
		}
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public int getIsCorrect() {
		return isCorrect;
	}
	public void setIsCorrect(int isCorrect) {
		this.isCorrect = isCorrect;
	}
	public int getExamquestionid() {
		return examquestionid;
	}
	public void setExamquestionid(int examquestionid) {
		this.examquestionid = examquestionid;
	}
	
	
	

}
