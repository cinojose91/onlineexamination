package Entity;

public class ExamPercentage {

	double pass;
	double fail;
	
	public ExamPercentage(double pass, double fail) {
		super();
		this.pass = pass;
		this.fail = fail;
	}
	
	public double getPass() {
		return pass;
	}
	public void setPass(double pass) {
		this.pass = pass;
	}
	public double getFail() {
		return fail;
	}
	public void setFail(double fail) {
		this.fail = fail;
	}
	@Override
	public String toString() {
		return "ExamPercentage [pass=" + pass + ", fail=" + fail + "]";
	}
	
	
}
