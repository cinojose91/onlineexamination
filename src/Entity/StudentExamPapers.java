package Entity;

import java.sql.Date;
import java.util.ArrayList;

public class StudentExamPapers extends ExamPaper {

	
	private int userid;
	private Date starttime;
	private int examscore;
	private int status=-1;
	private ArrayList<ExamQuestion> eqlist;
	
	public StudentExamPapers() {
		super();
	}
	public StudentExamPapers(int userid, Date starttime, int examscore, int status, ArrayList<ExamQuestion> eqlist) {
		super();
		this.userid = userid;
		this.starttime = starttime;
		this.examscore = examscore;
		this.status = status;
		this.eqlist = eqlist;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public Date getStarttime() {
		return starttime;
	}
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	public int getExamscore() {
		return examscore;
	}
	public void setExamscore(int examscore) {
		this.examscore = examscore;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public ArrayList<ExamQuestion> getEqlist() {
		return eqlist;
	}
	public void setEqlist(ArrayList<ExamQuestion> eqlist) {
		this.eqlist = eqlist;
	}
	
	@Override
	public String toString() {
		return "StudentExamPapers [userid=" + userid + ", starttime=" + starttime + ", examscore=" + examscore
				+ ", status=" + status + ", eqlist=" + eqlist + "]";
	}
	
}
