package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.ExamAnswer;
import Entity.ExamQuestion;
import Method.ExamQuestionController;

/**
 * Servlet implementation class ExamQuestionServlet
 */
@WebServlet("/ExamQuestionServlet")
public class ExamQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExamQuestionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
		ExamQuestion eq = new ExamQuestion();
		String txtQuestion = request.getParameter("txtQuestion");
		if(txtQuestion == null){
			txtQuestion="";
		}
		
		String examquestionid = request.getParameter("examquestionid");
		if(examquestionid == null){
			examquestionid="0";
		}
		int examqid= Integer.parseInt(examquestionid);
		
		String exampaperid = request.getParameter("exampaperid");
		if(exampaperid == null){
			exampaperid="1";
		}
		String score = request.getParameter("score");
		if(score == null){
			score="0";
		}
		
		ArrayList<ExamAnswer> examAnsList = new ArrayList<ExamAnswer>();
		for(int i=0; request.getParameter("txtAnswer"+i)!=null; i++){
			ExamAnswer ea = new ExamAnswer();
			String txtAnswer = request.getParameter("txtAnswer"+i);
			if(txtAnswer == null){
				txtAnswer="";
			}
			String chkCorrectAns = request.getParameter("chkCorrectAns"+i);
			if(chkCorrectAns == null){
				chkCorrectAns="0";
			}
			String answerId = request.getParameter("answerId"+i);
			if(answerId == null){
				answerId="0";
			}
			
			ea.setId(Integer.parseInt(answerId));
			ea.setAnswer(txtAnswer);
			ea.setIsCorrect(Integer.parseInt(chkCorrectAns));
			ea.setExamquestionid(examqid);
			examAnsList.add(ea);
		}
		
		String txtAnswer = request.getParameter("txtAnswer");
		if(txtAnswer == null){
			txtAnswer="";
		}
		boolean addnewans=false;
		
		if(!txtAnswer.trim().equals("")){
			ExamAnswer ea = new ExamAnswer();
			ea.setId(0);
			ea.setAnswer(txtAnswer);
			ea.setIsCorrect(0);
			ea.setExamquestionid(examqid);
			examAnsList.add(ea);
			addnewans= true;
		}
		
		eq.setId(examqid);
		eq.setQuestion(txtQuestion);
		eq.setExampaperid(Integer.parseInt(exampaperid));
		eq.setScore(Integer.parseInt(score));
		eq.setExamAnswerList(examAnsList);
		eq.setStatus(1);
		ExamQuestionController eqCtrl = new ExamQuestionController();
		eqCtrl.updateExamQuestion(eq);
		if(!addnewans){
		response.sendRedirect("ExamQuestion.jsp?action=update");
		}else{
		 response.sendRedirect("ExamQuestion.jsp?exampaperid="+eq.getExampaperid()+"&id="+eq.getId());
		}
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
	}

}
