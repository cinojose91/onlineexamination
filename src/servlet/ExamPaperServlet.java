package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.ExamPaper;
import Method.ExamPaperController;

/**
 * Servlet implementation class ExamPaperServlet
 */
@WebServlet("/ExamPaperServlet")
public class ExamPaperServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExamPaperServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		String txtPaperName = request.getParameter("txtPaperName");
		if(txtPaperName == null){
			txtPaperName="";
		}
		String txtPaperDescription = request.getParameter("txtPaperDescription");
		if(txtPaperDescription == null){
			txtPaperDescription="";
		}
		String txtPaperYear = request.getParameter("txtPaperYear");
		if(txtPaperYear == null){
			txtPaperYear="0";
		}
		int paperyear= Integer.parseInt(txtPaperYear);
		String txtPaperSemester = request.getParameter("txtPaperSemester");
		if(txtPaperSemester == null){
			txtPaperSemester="0";
		}
		int papersemester= Integer.parseInt(txtPaperSemester);
		String txtdurationHr = request.getParameter("txtdurationHr");
		if(txtdurationHr == null){
			txtdurationHr="0";
		}
		int examhours= Integer.parseInt(txtdurationHr);
		String txtdurationMin = request.getParameter("txtdurationMin");
		if(txtdurationMin == null){
			txtdurationMin="0";
		}
		int exammins= Integer.parseInt(txtdurationMin);
		String txtnoofquestions = request.getParameter("txtnoofquestions");
		if(txtnoofquestions == null){
			txtnoofquestions="0";
		}
		int noofquestions= Integer.parseInt(txtnoofquestions);

		String id = request.getParameter("id");
		String action ="";
		if(request.getParameter("action")!=null){
			action = request.getParameter("action");
			id="0";
		}
		if(id == null){
			id="1";
		}
		int exampaperid= Integer.parseInt(id);

		ExamPaper ep = new ExamPaper();
		ep.setName(txtPaperName);
		ep.setDescription(txtPaperDescription);
		ep.setYear(paperyear);
		ep.setSemester(papersemester);
		ep.setExamhours(examhours);
		ep.setExammins(exammins);
		ep.setId(exampaperid);		
		ep.setNoofquestions(noofquestions);
		ExamPaperController epCtrl = new ExamPaperController();
		try{

			epCtrl.updateExamQuestion(ep);

		}catch(Exception e){
			e.printStackTrace();
		}
		if(action.equalsIgnoreCase("a")){
			response.sendRedirect("ExamPapers.jsp");
		}
		else{
			response.sendRedirect("ExamPaperMaster.jsp?id="+id+"&action=update");
		}


	}

}
