package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.User;
import Method.UserController;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("null")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			UserController userCtrl = new UserController();
			
			String name = request.getParameter("fname");
			String userid = request.getParameter("userid");
			String password = request.getParameter("password");
			String email = request.getParameter("email");
			User user = new User();
			user.setName(name);
			user.setLoginid(userid);
			user.setEmail(email);
			user.setPassword(password);
			user.setStatus(1);
			System.out.println("Adding new user"+user);
			int rs = userCtrl.AddNewUser(user);
			if(rs > 1){
				userCtrl.addUserType(rs, 1);
				response.sendRedirect("login.jsp");
			}
		}catch (Exception e){
			System.out.println("Error in sign up"+e.getMessage());
		}
		
		
	}

}
