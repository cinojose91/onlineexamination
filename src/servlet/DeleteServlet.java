package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Method.ExamPaperController;
import Method.ExamQuestionController;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				String action = request.getParameter("action");
				String id = request.getParameter("id");
				if(id== null){
					id="0";
					
				}
				String examansid = request.getParameter("examansid");
				if(examansid== null){
					examansid="0";		
				}
				
				String examqnsid = request.getParameter("examqnsid");
				if(examqnsid== null){
					examqnsid="0";		
				}
				
				String exampaperid = request.getParameter("exampaperid");
				if(exampaperid== null){
					exampaperid="0";		
				}
				
				
				if(action.equals("deleteans")){
					int ansid =Integer.parseInt(examansid);
					ExamQuestionController examQuestionController = new ExamQuestionController();
					
					try {
						examQuestionController.deleteExamAnswerById(ansid);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					response.sendRedirect("ExamQuestion.jsp?exampaperid="+exampaperid+"&id="+id);
				}else if(action.equals("deleteqns")){
					int qnsid =Integer.parseInt(examqnsid);
					ExamQuestionController examQuestionController = new ExamQuestionController();
					try {
						examQuestionController.deleteExamQuestionById(qnsid);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					response.sendRedirect("ExamPaperMaster.jsp?id="+id);
					
				}else if(action.equals("deletepaper")){
					int paperid= Integer.parseInt(exampaperid);
					ExamPaperController examPaperCtrl = new ExamPaperController();
					
					try {
						examPaperCtrl.deleteExamPaperById(paperid);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					response.sendRedirect("ExamPapers.jsp");
					
				}
	}


}
