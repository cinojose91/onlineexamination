package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import Entity.User;
import Method.UserController;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id= request.getParameter("txtusername");
		String password = request.getParameter("txtpassword");
		UserController userCtrl = new UserController();
		HttpSession session = request.getSession();
		
		try{
			int userId=userCtrl.authenticate(id,password);
			
		if(userId != 0){
			
			session.setAttribute("userId", userId);
			session.setAttribute("username", userCtrl.getUserById(userId).getName());
			session.setAttribute("userresult", "");
			ArrayList<Integer> userAccessRights = userCtrl.getUserAccessByUserid(userId);
			for(int i : userAccessRights){
				session.setAttribute("usertype", i);
				break;
			}
			response.sendRedirect("dashboard.jsp");
			
			
		}else{
			
		    session.setAttribute("userresult", "invalid");
			response.sendRedirect("login.jsp#page-header");
		}
	  }catch(Exception e){
		  e.printStackTrace();
		  
	  }
	}

}
