package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Method.ExamController;

/**
 * Servlet implementation class HasExamStartedServlet
 */
@WebServlet("/HasExamStartedServlet")
public class HasExamStartedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HasExamStartedServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ExamController examCtrl = new ExamController();
		
		
		String userid = request.getParameter("userId");
		if(userid == null){
			userid="0";
		}
		
		int userId= Integer.parseInt(userid);
		String exampaperid = request.getParameter("exampaperId");
		if(exampaperid == null){
			exampaperid="0";
		}
		
		int examPaperId= Integer.parseInt(exampaperid);
		int status=-1;
		System.out.println("userid"+userid+" exampaperid"+exampaperid);
		try{
	     status =examCtrl.hasStartedExam(userId, examPaperId);
	     System.out.println("statuss:"+status);
	     if(status== -1){
				examCtrl.startExamination(userId,examPaperId);
	      }
		}catch(Exception e){
			e.printStackTrace();
		}
		
	
		PrintWriter out = response.getWriter();
		out.println(status);
		
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ExamController examCtrl = new ExamController();
		
		String userid = request.getParameter("userId");
		if(userid == null){
			userid="0";
		}
		
		int userId= Integer.parseInt(userid);
		String exampaperid = request.getParameter("exampaperId");
		if(exampaperid == null){
			exampaperid="0";
		}
		
		int examPaperId= Integer.parseInt(exampaperid);
		int status=-1;
		System.out.println("userid"+userid+" exampaperid"+exampaperid);
		try{
	     status =examCtrl.hasStartedExam(userId, examPaperId);
	     System.out.println("status:"+status);
	     if(status==-1){
				examCtrl.startExamination(userId,examPaperId);
	      }
		}catch(Exception e){
			e.printStackTrace();
		}
		
	
		PrintWriter out = response.getWriter();
		out.println(status);
		
	}*/

}
