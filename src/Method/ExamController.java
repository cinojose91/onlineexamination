package Method;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import Entity.ExamAnswer;
import Entity.ExamPaper;
import Entity.ExamQuestion;
import Entity.StudentExam;
import Util.DBConnection;

public class ExamController {
	
	public StudentExam getStudentExamQuestionByUseridExamPaperId(int userid, int exampaperid) throws Exception {
		StudentExam studentexam = new StudentExam();
    	
        try {
        	DBConnection dbconn = new DBConnection();
            String selectStatement = "select * from studentexaminfo where userid= ? and exampaperid=?";
            Connection con = dbconn.connect();
            
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
            prepStmt.setInt(1, userid);
            prepStmt.setInt(2, exampaperid);
            ResultSet rs = prepStmt.executeQuery();
            
            while (rs.next()) {
            	int id = rs.getInt("id");
            	String starttime = rs.getString("starttime");
            	String endtime = rs.getString("endtime");
            	String strquestionslist = rs.getString("questionslist");
            	int score = rs.getInt("score");
            	int status = rs.getInt("status");
            	studentexam.setId(id);
            	studentexam.setUserid(userid);
            	studentexam.setExampaperid(exampaperid);
            	studentexam.setStrquestionslist(strquestionslist);
            	studentexam.setStarttime(starttime);
            	studentexam.setEndtime(endtime);
            	studentexam.setScore(score);
            	studentexam.setStatus(status);
            	studentexam.setExamquestionlist(getStudentExamQuestionByUseridExamPaperId(con,strquestionslist));
            } 
            
            prepStmt.close();
            
            
            dbconn.close();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return studentexam;
    }
	
	public ArrayList<ExamQuestion> getStudentExamQuestionByUseridExamPaperId(Connection con, String strquestionlist) throws Exception {
		ArrayList<ExamQuestion> questionlist = new ArrayList<ExamQuestion>();
    	ExamQuestionController eqController = new ExamQuestionController();
        try {

            String selectStatement = "select * from examquestions where id in ("+strquestionlist+")";         
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
            
            ResultSet rs = prepStmt.executeQuery();
            
            while (rs.next()) {
            	ExamQuestion eq = new ExamQuestion();
            	int id = rs.getInt("id");
            	String question = rs.getString("question");
            	int exampaperid = rs.getInt("exampaperid");
            	int score = rs.getInt("score");
            	int status = rs.getInt("status");
         
            	eq.setId(id);
            	eq.setQuestion(question);
            	eq.setExampaperid(exampaperid);
            	eq.setScore(score);
            	eq.setStatus(status);
            	eq.setExamAnswerList(eqController.getExamAnsListByEQid(con,id));
            	questionlist.add(eq);        	
            } 
            
            prepStmt.close();
            
           
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return questionlist;
    }
	
	
	/*
	 * @parm1 userid eg: 1,2,3
	 * @param2 examid eg: 1, 2,3
	 * @return Status 
	 * 		1 - completed
	 * 		0 - progress
	 */
	public int hasStartedExam(int userid, int exampaperid) throws Exception {
		int examstatus =-1;
    	
        try {
        	DBConnection dbconn = new DBConnection();
            String selectStatement = "select status from studentexaminfo where userid= ? and exampaperid=?";
            Connection con = dbconn.connect();
            
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
            prepStmt.setInt(1, userid);
            prepStmt.setInt(2, exampaperid);
            ResultSet rs = prepStmt.executeQuery();
            
            if (rs.next()) {
            	int status = rs.getInt("status");
            	examstatus=status;
            	
            } 
            
            prepStmt.close();
            
            
            dbconn.close();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return examstatus;
    }
	
	public HashMap<Integer,Integer> getCorrectAnsByQuestionList(String questionlist) throws Exception {
		HashMap<Integer,Integer> correctAns = new HashMap<Integer,Integer>();
    	
        try {
        	DBConnection dbconn = new DBConnection();
            String selectStatement = "select id,examquestionid from examanswers where iscorrect=1 and examquestionid in ("+questionlist+")";
            System.out.println("correct ans sql:"+selectStatement);
            Connection con = dbconn.connect();
            
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
            ResultSet rs = prepStmt.executeQuery();
            
            while (rs.next()) {
             int id = rs.getInt("id");
             int examquestionid = rs.getInt("examquestionid");
             correctAns.put(examquestionid, id);
            } 
            
            prepStmt.close();
            
            
            dbconn.close();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return correctAns;
    }
	
	
public void startExamination(int userid, int exampaperid) throws Exception{
    	
 
        try {
        	DBConnection dbconn = new DBConnection();
        	
            String selectStatement = "insert into studentexaminfo (userid,exampaperid,starttime,questionslist) values (?,?,now(),?)";
            Connection con = dbconn.connect();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setInt(1, userid);
            prepStmt.setInt(2, exampaperid);
            prepStmt.setString(3, generateQuestionsList(con,exampaperid));
            prepStmt.executeUpdate();        
            prepStmt.close();
            

            
            con.close();
            
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
       
    }

public void endExamination(int studentexamid, int score, ArrayList<ExamAnswer> examanswerlist) throws Exception{
	
	
    try {
    	DBConnection dbconn = new DBConnection();
    	
        String selectStatement = "update studentexaminfo set endtime=now(), score=?, status=1 where id=?";
        Connection con = dbconn.connect();
        

        PreparedStatement prepStmt = con.prepareStatement(selectStatement);
        prepStmt.setInt(1, score);
        prepStmt.setInt(2, studentexamid);
        prepStmt.executeUpdate();
        prepStmt.close();
        insertStudentAns(con,examanswerlist,studentexamid);     
        con.close();
        
       
    } catch (SQLException ex) {
        ex.printStackTrace();
    }
    
   
}


public void insertStudentAns(Connection con,ArrayList<ExamAnswer> examanswerlist, int studentexamid ) throws Exception {
	
   try {

       String selectStatement = "insert into studentexamdetails (studentexamid,examquestionid,studentansid,iscorrect) values (?,?,?,?) ON DUPLICATE KEY "
       		+ " UPDATE studentansid= ? , iscorrect =?";
       PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
       for(int i=0;i<examanswerlist.size();i++){
    	   ExamAnswer studentans = examanswerlist.get(i);
    	   prepStmt.setInt(1, studentexamid);
	       prepStmt.setInt(2, studentans.getExamquestionid());
	       prepStmt.setInt(3, studentans.getId());
	       prepStmt.setInt(4, studentans.getIsCorrect());  
	       prepStmt.setInt(5, studentans.getId());
	       prepStmt.setInt(6, studentans.getIsCorrect());
	       prepStmt.executeUpdate();
       }
       
       prepStmt.close();
       
      
   } catch (SQLException ex) {
       ex.printStackTrace();
   }
   
}

public void insertAutoSaveAns(ArrayList<ExamAnswer> examanswerlist, int studentexamid) throws Exception {
	DBConnection dbconn = DBConnection.getInstance();
	Connection conn = dbconn.connect();
	try {

	       String selectStatement = "insert into studentexamdetails (studentexamid,examquestionid,studentansid,iscorrect) values (?,?,?,?) ON DUPLICATE KEY "
	       		+ " UPDATE studentansid= ? , iscorrect =?";
	       PreparedStatement prepStmt = conn.prepareStatement(selectStatement);   
	       for(int i=0;i<examanswerlist.size();i++){
	    	   ExamAnswer studentans = examanswerlist.get(i);
	    	   prepStmt.setInt(1, studentexamid);
		       prepStmt.setInt(2, studentans.getExamquestionid());
		       prepStmt.setInt(3, studentans.getId());
		       prepStmt.setInt(4, studentans.getIsCorrect());  
		       prepStmt.setInt(5, studentans.getId());
		       prepStmt.setInt(6, studentans.getIsCorrect());
		       prepStmt.executeUpdate();
	       }
	       
	       prepStmt.close();
	       
	      
	   } catch (SQLException ex) {
	       ex.printStackTrace();
	   }
	
}

 public String generateQuestionsList(Connection con,int exampaperid) throws Exception {
	String questionList ="";
	
    try {

        //String selectStatement = "SELECT noofquestions, max(q.id) as availquestions FROM `exampaperinfo` i, examquestions q where i.id=? and i.id= q.exampaperid group by noofquestions;";
    	String selectStatement = "SELECT noofquestions, q.id as questionsid FROM `exampaperinfo` i, examquestions q where i.id=? and q.status=1 and i.id= q.exampaperid;";
        
        PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
        prepStmt.setInt(1, exampaperid);
        ResultSet rs = prepStmt.executeQuery();
        ArrayList<Integer> examquestionlist = new ArrayList<Integer>();
        int noofquestions = 0;
        while (rs.next()) {
        	if(noofquestions==0){
        		noofquestions = rs.getInt("noofquestions");
            }
        	int questionsid = rs.getInt("questionsid");
        	examquestionlist.add(questionsid);
        	} 
        questionList=createQuestionOrder(noofquestions,examquestionlist);
        prepStmt.close();
        
        

       
    } catch (SQLException ex) {
        ex.printStackTrace();
    }
    
    return questionList;
}
 public String createQuestionOrder(int noofquestions, ArrayList<Integer> examquestionlist){
	 String questionList="";
	 int counter =0;
	
	 while (counter<noofquestions){
		 double q =  (Math.random()*examquestionlist.size());
		 int questionnoindex = (int) q;
		 int questionno=examquestionlist.get(questionnoindex);
		 
		 if(counter==0){
			 questionList= Integer.toString(questionno);
			 counter++;
		 }else if(counter==1){
			 if(Integer.parseInt(questionList) != questionno){
				 questionList= questionList +","+Integer.toString(questionno);
				 counter++; 
			 }
		 }else{

			 if(questionList.indexOf(","+questionno) == -1 && questionList.indexOf(questionno+",") == -1 ){
				 questionList= questionList +","+Integer.toString(questionno);
				 counter++; 
			 }	
		 }
				  
	 }
	 	
	
	 return questionList;
 }
 
 /*
  * Function to retrieve all the answers of the student.
  */

 public ArrayList<ExamAnswer> getStudentAnswer(int studentexamId) throws Exception{
	 ArrayList<ExamAnswer> list = new ArrayList<ExamAnswer>();
	 
	 try{
		 DBConnection dbconn = DBConnection.getInstance();
		 Connection conn = dbconn.connect();
		 String selectStatement =  " SELECT * FROM studentexamdetails WHERE studentexamid =?";
		 PreparedStatement prepStmt = conn.prepareStatement(selectStatement);   
	     prepStmt.setInt(1, studentexamId);
	     ResultSet rs = prepStmt.executeQuery();
	     while (rs.next()) {
	    	 ExamAnswer ea = new ExamAnswer();
	    	 ea.setExamquestionid(rs.getInt("examquestionid"));
	    	 ea.setId(rs.getInt("studentansid"));
	    	 list.add(ea);
	     }
	        
	 }catch(Exception e){
		e.printStackTrace(); 
	 }
	 
	 return list;
	 
 }
 
 /*
  * Function to retrieve the answer from list
  */
 
 public int getMyanswerId(ArrayList<ExamAnswer> list,int qnid){
	 int retval = 0;
	 for(ExamAnswer ea : list){
		if (ea.getExamquestionid() == qnid){
			retval= ea.getId();
		}
	 }
	 return retval;
 }
}
