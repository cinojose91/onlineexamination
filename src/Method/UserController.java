package Method;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import Entity.User;
import Util.DBConnection;

public class UserController {


	public int authenticate(String id, String password) throws Exception {
		int userid =0;

		try {
			DBConnection dbconn = new DBConnection();
			String selectStatement = "select id from user where loginid = ? and password = ?";
			Connection con = dbconn.connect();

			PreparedStatement prepStmt = con.prepareStatement(selectStatement);
			prepStmt.setString(1, id);
			prepStmt.setString(2, password);

			ResultSet rs = prepStmt.executeQuery();

			if (rs.next()) {
				userid = rs.getInt("id");           
			} 

			prepStmt.close();
			dbconn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return userid;
	}
	
	public User getUserById(int id) throws Exception {
		User user = new User();

		try {
			DBConnection dbconn = new DBConnection();
			String selectStatement = "select * from user where id = ?";
			Connection con = dbconn.connect();

			PreparedStatement prepStmt = con.prepareStatement(selectStatement);
			prepStmt.setInt(1, id);

			ResultSet rs = prepStmt.executeQuery();

			if (rs.next()) {
				String name = rs.getString("name");
				String loginid = rs.getString("loginid");
				String password = rs.getString("password");
				String email = rs.getString("email");
				user.setId(id);
				user.setName(name);
				user.setLoginid(loginid);
				user.setPassword(password);
				user.setEmail(email);
			} 

			prepStmt.close();
			dbconn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return user;
	}


	public int AddNewUser(User user) throws Exception {
		int id =0;
		try{
			DBConnection dbconn = new DBConnection();
			String insertstatement = "INSERT INTO user(name, loginid, password, email, status) values (?, ?, ?, ?, ?)";
			Connection con = dbconn.connect();

			PreparedStatement prepStmt = con.prepareStatement(insertstatement,Statement.RETURN_GENERATED_KEYS);
			prepStmt.setString(1, user.getName());
			prepStmt.setString(2, user.getLoginid());
			prepStmt.setString(3, user.getPassword());
			prepStmt.setString(4, user.getEmail());
			prepStmt.setInt(5, user.getStatus());
			id = prepStmt.executeUpdate();
			ResultSet rs = prepStmt.getGeneratedKeys();
		    rs.next();
		    id = rs.getInt(1);
			prepStmt.close();
			dbconn.close();

		}catch(Exception e){
			e.printStackTrace();
			throw new Exception(e);
		}

		return id;
	}
	
	/*
	 * Function to add user access
	 * @parm1 userid
	 * @param2 user type 
	 * 	Eg: 1 - student
	 *      2 - lecturer.
	 *      3 - Admin
	 */
	public void addUserType(int userid, int type) throws Exception{
		try{
			DBConnection dbconn = DBConnection.getInstance();
			String insertstatement = "INSERT INTO useraccess(userid, typeid) values (?, ?)";
			Connection con = dbconn.connect();

			PreparedStatement prepStmt = con.prepareStatement(insertstatement);
			prepStmt.setInt(1, userid);
			prepStmt.setInt(2, type);
			prepStmt.executeUpdate();
			prepStmt.close();
			dbconn.close();

		}catch(Exception e){
			e.printStackTrace();
			throw new Exception(e);
		}

	}
	
	public ArrayList<Integer> getUserAccessByUserid(int id) throws Exception {
		ArrayList<Integer> userAccessRights= new ArrayList<Integer>();

		try {
			DBConnection dbconn = new DBConnection();
			String selectStatement = "SELECT distinct typeid FROM useraccess where userid=?;";
			Connection con = dbconn.connect();

			PreparedStatement prepStmt = con.prepareStatement(selectStatement);
			prepStmt.setInt(1, id);

			ResultSet rs = prepStmt.executeQuery();

			if (rs.next()) {
				int typeid = rs.getInt("typeid");
				userAccessRights.add(typeid);
			} 

			prepStmt.close();
			dbconn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return userAccessRights;
	}

}
