package Method;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Entity.ExamAnswer;
import Entity.ExamQuestion;
import Util.DBConnection;

public class ExamQuestionController {

	public void updateExamQuestion(ExamQuestion eq) throws Exception{
    	
    	
        try {
        	DBConnection dbconn = new DBConnection();
        	
            String selectStatement = "insert into examquestions (question,exampaperid,score,status) values (?,?,?,?)";
            Connection con = dbconn.connect();
            
            if(eq.getId()==0){
            PreparedStatement prepStmt = con.prepareStatement(selectStatement , PreparedStatement.RETURN_GENERATED_KEYS);
            prepStmt.setString(1, eq.getQuestion());
            prepStmt.setInt(2, eq.getExampaperid());
            prepStmt.setInt(3, eq.getScore());
            prepStmt.setInt(4, eq.getStatus());
            prepStmt.executeUpdate();
            
           ResultSet rs = prepStmt.getGeneratedKeys();

            if (rs.next()) {
                eq.setId(rs.getInt(1));
            }
            
            prepStmt.close();
            
           }else {
        	   selectStatement = "update examquestions set question=?,exampaperid=?,score=?,status=? where id=?";
        	   PreparedStatement prepStmt = con.prepareStatement(selectStatement);
               prepStmt.setString(1, eq.getQuestion());
               prepStmt.setInt(2, eq.getExampaperid());
               prepStmt.setInt(3, eq.getScore());
               prepStmt.setInt(4, eq.getStatus());
               prepStmt.setInt(5, eq.getId());
               prepStmt.executeUpdate();
               prepStmt.close();
                 
           }
            updateExamAnswer(con,eq.getExamAnswerList(),eq.getId());
            
            con.close();
            
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
       
    }
	
	public void updateExamAnswer(Connection con ,ArrayList<ExamAnswer> examAnsList, int examQuestionId ) throws Exception{

    	
        try {

        	
            String selectStatement = "";
      
            PreparedStatement prepStmt = null;
            for(int i=0;i<examAnsList.size();i++){
            	ExamAnswer ea= examAnsList.get(i);
            if(ea.getId() == 0){
            selectStatement = "insert into examanswers (examquestionid,answer,iscorrect) values (?,?,?)";
            prepStmt = con.prepareStatement(selectStatement , PreparedStatement.RETURN_GENERATED_KEYS);
            prepStmt.setInt(1, examQuestionId);
            prepStmt.setString(2, ea.getAnswer());
            prepStmt.setInt(3, ea.getIsCorrect());
            prepStmt.executeUpdate();
            
           ResultSet rs = prepStmt.getGeneratedKeys();

            if (rs.next()) {
                ea.setId(rs.getInt(1));
            }
            
            prepStmt.close();
            
           }else {
        	   selectStatement = "update examanswers set examquestionid=?,answer=?,iscorrect=? where id=?";
        	   prepStmt = con.prepareStatement(selectStatement);
        	   prepStmt.setInt(1, ea.getExamquestionid());
               prepStmt.setString(2, ea.getAnswer());
               prepStmt.setInt(3, ea.getIsCorrect());
               prepStmt.setInt(4, ea.getId());
               prepStmt.executeUpdate();
               prepStmt.close();
                 
           }
            
        } 
           
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        
    }
	
	public ExamQuestion getExamQuestionById(int id) throws Exception {
		ExamQuestion eq = new ExamQuestion();
    	
        try {
        	DBConnection dbconn = new DBConnection();
            String selectStatement = "select * from examquestions where id = ?";
            Connection con = dbconn.connect();
            
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
            prepStmt.setInt(1, id);
            ResultSet rs = prepStmt.executeQuery();
            
            if (rs.next()) {
            	String question = rs.getString("question");
            	int exampaperid = rs.getInt("exampaperid");
            	int score = rs.getInt("score");
            	int status = rs.getInt("status");
            	eq.setQuestion(question);
            	eq.setScore(score);
            	eq.setStatus(status);
            	eq.setId(id);
            	eq.setExamAnswerList(getExamAnsListByEQid(con,id));
            } 
            
            prepStmt.close();
            
            
            dbconn.close();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return eq;
    }
	public ArrayList<ExamAnswer> getExamAnsListByEQid(Connection con,int id) throws Exception {
		ArrayList<ExamAnswer>  eaList = new ArrayList<ExamAnswer>();
    	
        try {
            String selectStatement = "select id,answer,iscorrect from examanswers where examquestionid = ?";

            
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
            prepStmt.setInt(1, id);
            ResultSet rs = prepStmt.executeQuery();
            
            while (rs.next()) {
            	ExamAnswer ea = new ExamAnswer();
            	int eaid = rs.getInt("id");
            	String answer = rs.getString("answer");
            	int iscorrect = rs.getInt("iscorrect");
            	ea.setAnswer(answer);
            	ea.setIsCorrect(iscorrect);
            	ea.setExamquestionid(id);
            	ea.setId(eaid);
            	eaList.add(ea);
            } 
            
            prepStmt.close();
            
           
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return eaList;
    }
	
	public void deleteExamAnswerById(int id) throws Exception {
			try{
				DBConnection dbconn = DBConnection.getInstance();
				String sql = "delete from examanswers where id=? ";
				Connection con = dbconn.connect();

				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setInt(1, id);
				prepStmt.executeUpdate();
				prepStmt.close();
				dbconn.close();
				
			}catch(Exception e){
				e.printStackTrace();
				throw new Exception(e);
			}

   }
   
	public void deleteExamQuestionById(int id) throws Exception {
		try{
			DBConnection dbconn = DBConnection.getInstance();
			String sql = "update examquestions set status=0 where id=? ";
			Connection con = dbconn.connect();

			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, id);
			prepStmt.executeUpdate();
			prepStmt.close();
			dbconn.close();
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception(e);
		}

}
	
	
	
}
