package Method;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Entity.ExamPaper;
import Entity.ExamQuestion;
import Entity.StudentExamPapers;
import Util.DBConnection;
import Entity.ExamPercentage;

public class ExamPaperController {

	public ExamPaper getPaperById(int id) throws Exception {
		ExamPaper ep = new ExamPaper();
    	
        try {
        	DBConnection dbconn = new DBConnection();
            String selectStatement = "select * from exampaperinfo where id = ?";
            Connection con = dbconn.connect();
            
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
            prepStmt.setInt(1, id);
            ResultSet rs = prepStmt.executeQuery();
            
            if (rs.next()) {
            	
            	String name = rs.getString("name");
            	String description = rs.getString("description");
            	int semester = rs.getInt("semester");
            	int year = rs.getInt("year");
            	int durationhr = rs.getInt("durhours");
            	int durationmin = rs.getInt("durmins");
            	int status = rs.getInt("status");
            	int noofquestions = rs.getInt("noofquestions");
            	ep.setName(name);
            	ep.setDescription(description);
            	ep.setYear(year);
            	ep.setSemester(semester);
            	ep.setExamhours(durationhr);
            	ep.setExammins(durationmin);
            	ep.setStatus(status);
            	ep.setId(id);
            	ep.setNoofquestions(noofquestions);
            	ep.setEqlist(getExamQuestionsListByExamPaperId(con,id));
            	
            } 
            
            prepStmt.close();
            
            
            dbconn.close();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return ep;
    }
	
	
	public ArrayList<ExamQuestion> getExamQuestionsListByExamPaperId(Connection con,int exampaperid) throws Exception {
		ArrayList<ExamQuestion> eqList = new ArrayList<ExamQuestion>();
    	
        try {

            String selectStatement = "select * from examquestions where exampaperid=? and status=1";
         
            
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);   
            prepStmt.setInt(1, exampaperid);
            ResultSet rs = prepStmt.executeQuery();
            
            while (rs.next()) {
            	ExamQuestion eq = new ExamQuestion();
            	int id = rs.getInt("id");
            	String question = rs.getString("question");
            	int score = rs.getInt("score");
            	int status = rs.getInt("status");
            	eq.setQuestion(question);
            	eq.setScore(score);
            	eq.setStatus(status);
            	eq.setId(id);
            	eqList.add(eq);

            } 
            
            prepStmt.close();
            
            
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return eqList;
    }
	
public void updateExamQuestion(ExamPaper ep) throws Exception{
    	
    	
        try {
        	DBConnection dbconn = new DBConnection();
        	
            String selectStatement = "insert into exampaperinfo (name,description,year,semester,durhours,durmins,status,noofquestions) values (?,?,?,?,?,?,1,?)";
            Connection con = dbconn.connect();
            
            if(ep.getId()==0){
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, ep.getName());
            prepStmt.setString(2, ep.getDescription());
            prepStmt.setInt(3, ep.getYear());
            prepStmt.setInt(4, ep.getSemester());
            prepStmt.setInt(5, ep.getExamhours());
            prepStmt.setInt(6, ep.getExammins());
            prepStmt.setInt(7, ep.getNoofquestions());
            prepStmt.executeUpdate();                   
            prepStmt.close();         
           }else {
        	   selectStatement = "update exampaperinfo set name=?,description=?,year=?,semester=?,durhours=?,durmins=?,status=1,noofquestions=? where id=?";
        	   PreparedStatement prepStmt = con.prepareStatement(selectStatement);
        	   prepStmt.setString(1, ep.getName());
               prepStmt.setString(2, ep.getDescription());
               prepStmt.setInt(3, ep.getYear());
               prepStmt.setInt(4, ep.getSemester());
               prepStmt.setInt(5, ep.getExamhours());
               prepStmt.setInt(6, ep.getExammins());
               prepStmt.setInt(7, ep.getNoofquestions());
               prepStmt.setInt(8, ep.getId());
               prepStmt.executeUpdate();
               prepStmt.close();
                 
           }

            
            con.close();
            
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
       
    }

public ArrayList<ExamPaper> getAvailablePapers() throws Exception{
	
	ArrayList<ExamPaper> list = new ArrayList<ExamPaper>();
	try{
		DBConnection dbconn = DBConnection.getInstance();
		Connection conn = dbconn.connect();
//		String selectStatement = "select count(*) as count, ei.* from examquestions eq "
//				+ "left join exampaperinfo  ei on eq.exampaperid = ei.id group by eq.exampaperid;";
		String selectStatement = "select * from exampaperinfo where status=1";
		PreparedStatement prepStmt = conn.prepareStatement(selectStatement);   
        ResultSet rs = prepStmt.executeQuery();
		
        while(rs.next()){
        	ExamPaper ep = new ExamPaper();
        	ep.setId(rs.getInt("id"));
        	ep.setName(rs.getString("name"));
        	ep.setExammins(rs.getInt("durmins"));
        	ep.setDescription(rs.getString("description"));
        	ep.setNoofquestions(rs.getInt("noofquestions"));
        	list.add(ep);
        	
        }
        
       conn.close(); 
	}catch(Exception e){
		e.printStackTrace();
	}
	
	
	return list;
}

@SuppressWarnings("null")
public ExamPercentage getPercentage(int exampaperid) throws Exception{
	ExamPercentage ep = new ExamPercentage(0, 0);
	try{
		DBConnection dbconn = DBConnection.getInstance();
		Connection conn = dbconn.connect();
		String selectstatement = "select * from resultstat where exampaperid=?";
		PreparedStatement prepStmt = conn.prepareStatement(selectstatement);  
		prepStmt.setInt(1, exampaperid);
        ResultSet rs = prepStmt.executeQuery();
        if(rs.next()){
        	int passcount = rs.getInt("passcount");
        	int failcount = rs.getInt("failcount");
        	int total = rs.getInt("total");
        	if(total > 0 ){
        	double pass = ((double)passcount/(double)total)*100;
        	double fail = ((double)failcount/(double)total)*100;
        	ep= new ExamPercentage(pass, fail);
        	}
        	else{
        		
        		ep = new ExamPercentage(0, 0);
        	}
        }
        
	}catch(Exception e){
		e.printStackTrace();
	}
	return ep;
}
public ArrayList<StudentExamPapers> getStudentModuleList(int id) throws Exception{
	
	ArrayList<StudentExamPapers> list = new ArrayList<StudentExamPapers>();
	try{
		DBConnection dbconn = DBConnection.getInstance();
		Connection conn = dbconn.connect();
		String selectStatement = "select * from exampaperinfo where status=1";
		PreparedStatement prepStmt = conn.prepareStatement(selectStatement);   
        ResultSet rs = prepStmt.executeQuery();
		
        while(rs.next()){
        	StudentExamPapers ep = new StudentExamPapers();
        	ep.setId(rs.getInt("id"));
        	ep.setName(rs.getString("name"));
        	ep.setExammins(rs.getInt("durmins"));
        	ep.setDescription(rs.getString("description"));
        	ep.setNoofquestions(rs.getInt("noofquestions"));
            ep =  getExamDetail(ep, id);
        	list.add(ep);
        	
        }
        
       conn.close(); 
	}catch(Exception e){
		e.printStackTrace();
	}
	return list;
}

public StudentExamPapers getExamDetail(StudentExamPapers ep,int stid) throws Exception{
	
	DBConnection dbconn = DBConnection.getInstance();
	Connection conn = dbconn.connect();
	String selectStatement = "select * from studentexaminfo where exampaperid=? and userid=?";
	PreparedStatement prepStmt = conn.prepareStatement(selectStatement);
	prepStmt.setInt(1, ep.getId());
    prepStmt.setInt(2, stid);
	ResultSet rs = prepStmt.executeQuery();
	if(rs.next()){
		ep.setStatus(rs.getInt("status"));
		ep.setStarttime(rs.getDate("starttime"));
		ep.setExamscore(rs.getInt("score"));
	}
	return ep;
}

public void deleteExamPaperById(int id) throws Exception {
	try{
		DBConnection dbconn = DBConnection.getInstance();
		String sql = "update exampaperinfo set status=0 where id=? ";
		Connection con = dbconn.connect();

		PreparedStatement prepStmt = con.prepareStatement(sql);
		prepStmt.setInt(1, id);
		prepStmt.executeUpdate();
		prepStmt.close();
		dbconn.close();
		
	}catch(Exception e){
		e.printStackTrace();
		throw new Exception(e);
	}

}
	
}
	

