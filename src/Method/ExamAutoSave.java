package Method;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.ExamAnswer;

/**
 * Servlet implementation class ExamAutoSave
 */
@WebServlet("/ExamAutoSave")
public class ExamAutoSave extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExamAutoSave() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id= request.getParameter("txtId");
		if(id==null){
			id="0";
		}
		int studentexamid= Integer.parseInt(id);

		String noOfQuestion= request.getParameter("txtNoOfQuestion");
		if(noOfQuestion==null){
			noOfQuestion="0";
		}
		int noofquestion= Integer.parseInt(noOfQuestion);

		String txtExamQuestionsList= request.getParameter("txtExamQuestionsList");
		if(txtExamQuestionsList==null){
			txtExamQuestionsList="";
		}
		ExamController examCtrl = new ExamController();
		HashMap<Integer,Integer> correctAns = null;
		try{
			correctAns = examCtrl.getCorrectAnsByQuestionList(txtExamQuestionsList);
		}catch(Exception e){
			e.printStackTrace();
		}

		double totalscore = 0;
		double maxexamscore=0;

		ArrayList<ExamAnswer> examAnswerList =new ArrayList<ExamAnswer>();
		for(int i=0;request.getParameter("txtExamQuestionId"+i) != null;i++){ //question will always be equal = ans
			ExamAnswer ea = new ExamAnswer();
			String txtExamQuestionId = request.getParameter("txtExamQuestionId"+i);
			int examquestionid = Integer.parseInt(txtExamQuestionId);
			if(request.getParameter("rdAns"+i) == null ) continue;
			String studentAnsId = request.getParameter("rdAns"+i);
			int studentansid =Integer.parseInt(studentAnsId);
			ea.setId(studentansid);
			ea.setExamquestionid(examquestionid);

			int correctans = 0;
			if(correctAns.get(examquestionid) != null){
				correctans=correctAns.get(examquestionid) ;
			}
			String txtExamQuestionScore = request.getParameter("txtExamQuestionScore"+i);
			int examquestionscore = Integer.parseInt(txtExamQuestionScore);
			maxexamscore+=examquestionscore;

			if(correctans == studentansid ){
				ea.setIsCorrect(1); // ans is correct
				int score=examquestionscore;
				totalscore+=score;
			}else{
				ea.setIsCorrect(0); // ans is wrong
			}
			System.out.println("questiondid:"+examquestionid + " studentAnsId:"+studentAnsId + " isCorrect:"+ea.getIsCorrect());
			examAnswerList.add(ea);

		}
		double scorePercent = totalscore/maxexamscore*100;
		try{
			//examCtrl.insertAutoSaveAns(examAnswerList, studentexamid);
			examCtrl.endExamination(studentexamid, (int) Math.round(scorePercent), examAnswerList);
		}catch(Exception e){
			e.printStackTrace();
		}


		System.out.println("scorePercent:"+scorePercent);
		

	}


}
