package Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	Connection con;
   

	private boolean conFree = true;
    
    static DBConnection dbinstance= null;
    
    // Database configuration
    public static String url = "jdbc:mysql://awsdb.cfy98rgprjv4.ap-southeast-1.rds.amazonaws.com/onlineexam";
    public static String dbdriver = "com.mysql.jdbc.Driver";
    public static String username = "awsdbsa";
    public static String password = "Password!";
    
    public DBConnection() throws Exception {
      
    }
    
    public static DBConnection getInstance() throws Exception{
    	if(dbinstance == null){
    		dbinstance = new DBConnection();
    		return dbinstance;
    	}
    	else{
    		return dbinstance;
    	}
    }
    public void close() {
        try {
            con.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public Connection connect() {
    	  try {
              /*
              Context initCtx = new InitialContext();
              Context envCtx = (Context) initCtx.lookup("java:comp/env");
              DataSource ds = (DataSource) envCtx.lookup("jdbc/BookDB");
              con = ds.getConnection();
              */
              Class.forName(dbdriver);
              con = DriverManager.getConnection(url, username, password);
              
          } catch (Exception ex) {
              System.out.println("Exception in DBConnection: " + ex);
            
          }
        
        return con;
    }

    public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}
    
    
    
    

}
