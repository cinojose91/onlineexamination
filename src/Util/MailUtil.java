package Util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import java.net.URLEncoder;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;

public class MailUtil {
	
	private String host= "smtp.gmail.com";
	private String user= "internetprogramming.ntu@gmail.com";
	private String password= "yrkxqeqylspiripw";
	private String fromAddress= "internetprogramming.ntu@gmail.com";
    private String port = "587";
    
    public void sendMail(String to, String subject,String content, String attachmentLink){

		Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", host);
		//props.put("mail.smtp.socketFactory.port", "465"); //SSL Port
        //props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable","true");
		
		try{
			
			//create Authenticator object to pass in Session.getInstance argument
	        Authenticator auth = new Authenticator() {
	            //override the getPasswordAuthentication method
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(fromAddress, password);
	            }
	        };
			Session mailSession = Session.getInstance(props,null);
			//mailSession.setDebug(true);
			Message message=new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(fromAddress));

            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
                        // create and fill the first message part
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setHeader("Content-Type", "text/html");
            mbp1.setText(content);
            

            // create the second message part
            MimeBodyPart mbp2 = new MimeBodyPart();

            // attach the file to the message
            FileDataSource fds = new FileDataSource(attachmentLink);
            mbp2.setDataHandler(new DataHandler(fds));
            mbp2.setFileName(fds.getName());

            // create the Multipart and add its parts to it
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
            mp.addBodyPart(mbp2);


			message.setSubject(subject);
            message.setContent(mp, "text/html");
			message.saveChanges();


   
			
			Transport transport = mailSession.getTransport("smtp");
			
			transport.connect(host,Integer.parseInt(port) , user, password);
			System.out.println("mail");
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
	}
    
    public static void main(String[] args){
        MailUtil mUtil = new MailUtil();
        mUtil.sendMail("gilbertchee@bizmann.com","Subject","Content","C:\\Users\\Gilbert\\Desktop\\test.pptx");
        System.out.println("mail sent");
    }

}
