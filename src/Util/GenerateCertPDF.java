/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.*;

import Method.ExamPaperController;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;

/**
 *
 * @author Gilbert Chee
 */
public class GenerateCertPDF {
     Float imgsize= 60F;
     String exampaper="";
  /*  public static void main(String[] args){
  
       
    }*/
    
    public void createPdfForCert(int id,String name,String email,int exampaperid) {
       
        
        try {
            int margin = 20;
            ExamPaperController exampaperCtrl = new ExamPaperController();
            exampaper = exampaperCtrl.getPaperById(exampaperid).getName();
            String directory="output";
            String output =directory+exampaperid+name+id+".pdf"; //make sure cert folder is created or define a place to map the pdf
            File checkdirectory = new File(directory);
            if(!checkdirectory.isDirectory()){
            	checkdirectory.mkdir();
            }
            Document document = new Document();
            document.setMargins(margin, margin, margin, 50); // left,right,top, bottom
            FileOutputStream fileoutput = new FileOutputStream(output);
            PdfWriter writer = PdfWriter.getInstance(document,fileoutput );
           // writer.setPageEvent(new PdfEventHelper(staffClaimInfo, type, approvalInfo));
            document.open();
            document.add(formHeader(name));
            document.close();
            
            
            MailUtil mailutil = new MailUtil();
            String content="Dear "+name +", \r Congratulation you have passed our examination.\r Please refer to attachment for your certificate. \r Thank you \r Best Regards, \r Training Institution";
            mailutil.sendMail(email, "Course Certificate", content, output);
            
       }catch(Exception e){
    	   e.printStackTrace();
       }

     }

  public PdfPTable formHeader(String name)
    {
        
        //float[] hcolWidth = {1f, 1f, 1f, 1f, 1f, 1f,1f};
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100.0f);
        //PdfPTable table = new PdfPTable(5);
        
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String currentdate =dateFormat.format(date);
        try
        {
            URL url = this.getClass().getResource("/logo.jpg");
            Image awtImage =ImageIO.read(new File(url.toURI()));
        	com.lowagie.text.Image img = com.lowagie.text.Image.getInstance(awtImage, null);
            img.scalePercent(50F);
            
            PdfPCell cell = new PdfPCell(img, false);
            cell.setBorder(0);
           table.addCell(cell); //img
           
           URL stampurl = this.getClass().getResource("/rubberstamp.jpg");
           Image awtImagestamp =ImageIO.read(new File(stampurl.toURI()));
           com.lowagie.text.Image stampimg = com.lowagie.text.Image.getInstance(awtImagestamp, null);
           stampimg.scalePercent(60F);
           
           PdfPCell stampcell = new PdfPCell(stampimg, false);
           stampcell.setBorder(0);
           
           URL sigurl = this.getClass().getResource("/signature.jpg");
           Image sigimg =ImageIO.read(new File(sigurl.toURI()));
           com.lowagie.text.Image footerimg = com.lowagie.text.Image.getInstance(sigimg, null);
           footerimg.scalePercent(60F);
           
           PdfPCell footercell = new PdfPCell(footerimg, false);
           footercell.setBorder(0);
          
           
            
            PdfPCell emptyRow = new PdfPCell();
            emptyRow.setBorder(0);
            emptyRow.setFixedHeight(20F);
            table.addCell(emptyRow); 
            
           
            
            table.addCell(CreatePdfFunctions.ModifyContentWithColspan("Training Institution Level 1 Certificate in "+exampaper+" :",0)); //bold

            table.addCell(emptyRow); 
             
            
            table.addCell(CreatePdfFunctions.ModifyFontWithColspan("This is to certify that:",0));// small
           
            table.addCell(CreatePdfFunctions.ModifyContentWithColspan(name ,0)); //bold
            
            table.addCell(CreatePdfFunctions.ModifyFontWithColspan("has been awarded:",0));//small
            
            table.addCell(CreatePdfFunctions.ModifyContentWithColspan("PASS",0));//bold
            
            table.addCell(CreatePdfFunctions.ModifyFontWithColspan("in",0));//small
            
            table.addCell(CreatePdfFunctions.ModifyContentWithColspan("Certificate in "+exampaper,0));//bold
            
            table.addCell(emptyRow); 
            
            table.addCell(CreatePdfFunctions.ModifyFontWithColspan("on" ,0));//small
            
            table.addCell(CreatePdfFunctions.ModifyContentWithColspan(currentdate ,0));//bold
            
            table.addCell(emptyRow);
            
            table.addCell(stampcell); //stampimg
            
            table.addCell(emptyRow);
            
            table.addCell(footercell); //footerimg
            
                    
        }
        catch(Exception e) { 
			System.out.println("Exception in header : " + e);
			e.printStackTrace();
		}
        return table;
    }
  
 
  
   
   
  
}
