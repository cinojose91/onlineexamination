/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.awt.PrintJob.*;
import java.awt.PrintGraphics.*;
import java.awt.*;
import javax.print.DocFlavor.*;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;



/**
 *
 * @author Khin Thara Phu Nyein
 */

 
public class CreatePdfFunctions {
    static int fontSize = 14;
    public static Font ModifyHeaderFont(int fz) {
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"),fz,Font.BOLD);
        f.setColor(Color.black);
        return f;

    }
    
    public static Font chineseCharFont(int fontsize) throws Exception {
        //C:\Windows\Fonts\Microsoft YaHei Regular
        Font f = new Font(BaseFont.createFont("C:\\Windows\\Fonts\\msyh.ttf",BaseFont.IDENTITY_H,BaseFont.EMBEDDED), fontsize);
        
        return f;
    }

    public static PdfPCell ModifyContent(String s) {
        
        //Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"), fontSize, Font.BOLD);
        PdfPCell cell= null;
        try{
            Font f = chineseCharFont(fontSize);
            cell = new PdfPCell(new Paragraph(s, f));
            cell.setBorderColorBottom(Color.black);
            cell.setBorderColorTop(Color.black);
            cell.setBorderColorLeft(Color.black);
            cell.setBorderColorRight(Color.black);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        }catch(Exception e){
        e.printStackTrace();
        }
        return cell;
    }
    
    public static PdfPCell ModifyContentWithColspan(String s,int colspan) {        
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"), 18, Font.BOLD);
        PdfPCell cell = null;   
        try{
           // Font f = chineseCharFont(fontSize);
            cell = new PdfPCell(new Paragraph(s, f));
            cell.setColspan(colspan);
            cell.setFixedHeight(40f);
            cell.setBorder(0);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
           }catch(Exception e){
             e.printStackTrace();
            }
        return cell;
    }
    
    
     public static PdfPCell ModifyDetailsWithColspan(String s,int colspan,String align) {        
        //Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"), fontSize, Font.BOLD);
        PdfPCell cell= null;
        try{
        Font f = chineseCharFont(fontSize);    
        cell = new PdfPCell(new Paragraph(s, f));
        cell.setColspan(colspan);
        //cell.setBorder(PdfPCell.NO_BORDER); 
        if(align.equals("left")){
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        }else if(align.equals("right")){
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        }else{
         cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        }
       }catch(Exception e){
           e.printStackTrace();
       }
        return cell;
    }

    public static Font ModifyAddFont(int fz) {
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"),fz,Font.BOLD);
        f.setColor(Color.black);
        return f;

    }

    public static PdfPCell ModifyFont(String s) {
        PdfPCell cell= null;
        try{
        Font f = chineseCharFont(fontSize);    
        //Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"));
        //f.setSize(fontSize);
        cell = new PdfPCell(new Paragraph(s, f));        
        cell.setBorderColorBottom(Color.black);
        cell.setBorderColorTop(Color.black);
        cell.setBorderColorLeft(Color.black);
        cell.setBorderColorRight(Color.black);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        }catch(Exception e){
           e.printStackTrace();
       }
        return cell;
    }
    public static PdfPCell ModifyFontWithColspan(String s,int colspan) {
       PdfPCell cell= null;
        try{
        //Font f = chineseCharFont(fontSize);  
        
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"));
        f.setSize(fontSize);
        cell = new PdfPCell(new Paragraph(s, f));   
        cell.setBorder(0);
        cell.setFixedHeight(40f);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        }catch(Exception e){
           e.printStackTrace();
       }
        return cell;
    }
    
    public static PdfPCell ModifyFooterWithColspan(String s,int colspan, String align) {
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"));
        f.setSize(20);
        PdfPCell cell = new PdfPCell(new Paragraph(s, f));       
        cell.setColspan(colspan);
        cell.setBorder(PdfPCell.NO_BORDER); 
        
        if(align.equals("left")){
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        }else if(align.equals("right")) {
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        }else{
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        }
        
        return cell;
    }
      public static PdfPCell ModifyEmptyItemWithColspan(String s,int colspan, String align) {
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"));
        f.setSize(fontSize);
        PdfPCell cell = new PdfPCell(new Paragraph(s, f));       
        cell.setColspan(colspan);
        //cell.setBorder(PdfPCell.NO_BORDER); 
         cell.setBorder(PdfPCell.NO_BORDER);
            
        if(align.equals("left")){
        cell.setBorder(Rectangle.OUT_RIGHT);
        
        }else if(align.equals("right")){
        cell.setBorder(Rectangle.OUT_BOTTOM);
        }else if(align.equals("top")){
        cell.setBorder(Rectangle.OUT_LEFT);
        }
        
        return cell;
    }
   
    
     public static PdfPCell ModifyHeaderWithColspan(String s,int colspan) {         
        PdfPCell cell = new PdfPCell(new Paragraph(s, ModifyHeaderFont(fontSize)));
        cell.setColspan(colspan);
        //cell.setFixedHeight(12);
        cell.setBackgroundColor(Color.decode("#A5D3EC"));
        
        cell.setBorderColorTop(Color.black);
        cell.setBorderColorLeft(Color.black);
        cell.setBorderColorRight(Color.black);
        cell.setBorderColorBottom(Color.white);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return cell;
    }
     
        public static PdfPCell ModifyTitleWithColspan(String s,int colspan,String align) {         
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"),Font.BOLD);
        f.setSize(14);
        PdfPCell cell = new PdfPCell(new Paragraph(s, f));
        
        cell.setColspan(colspan);
        cell.setFixedHeight(20);
       // cell.setBackgroundColor(Color.decode("#A5D3EC"));
        cell.setBorder(PdfPCell.NO_BORDER);

        if(align.equals("left")){
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        }else if (align.equals("right")){
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        }
        return cell;
    }
        public static PdfPCell ModifyInvoiceNoWithColspan(String s,int colspan,String align) {         
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"),Font.BOLD);
        f.setSize(12);
        PdfPCell cell = new PdfPCell(new Paragraph(s, f));
        
        cell.setColspan(colspan);
        cell.setFixedHeight(18);
        cell.setBackgroundColor(Color.decode("#A5D3EC"));
        

        if(align.equals("left")){
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        }else if (align.equals("right")){
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        }
        return cell;
    }
    
    
    public static PdfPCell ModifySubHeaderWithColspan(String s,int colspan) {
        PdfPCell cell = new PdfPCell(new Paragraph(s, ModifyHeaderFont(fontSize)));
        cell.setColspan(colspan);
        //cell.setFixedHeight(12);
        cell.setBackgroundColor(Color.decode("#A5D3EC"));
        cell.setBorderColorTop(Color.black);
        cell.setBorderColorLeft(Color.black);
        cell.setBorderColorRight(Color.black);
        cell.setBorderColorBottom(Color.white);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        return cell;
    }
    
     public static PdfPCell ModifySignatureText() {               
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"), fontSize, Font.BOLD);
        PdfPCell text = new PdfPCell(new Paragraph("\n \n Signatory/Company Stamp", f)); 
        text.setColspan(2);
        text.setBorderColor(Color.black);
        text.setHorizontalAlignment(Element.ALIGN_LEFT);
        text.setFixedHeight(50);
        return text;
    }
     
     public static PdfPCell ModifiySign(int colspan){
        PdfPCell sign = new PdfPCell();
        sign.setColspan(colspan);
        sign.setBorderColor(Color.black);
        sign.setHorizontalAlignment(Element.ALIGN_LEFT);
        sign.setFixedHeight(20);
        return sign;
     }

    public static Font ModifyFontSize(int fz) {
        Font f = new Font(Font.getFamilyIndex("arial,helvetica,clean,sans-serif"));
        f.setSize(fz);
        return f;
    }
    
    public static PdfPTable getCreditCardDetailTable(String ccType,String ccNum,String ccSecCode, String ccExpDate){
        PdfPTable table = new PdfPTable(12);
        
        table.addCell(ModifySubHeaderWithColspan("Credit Card Details ",12));

        table.addCell(ModifyContentWithColspan("Credit Card Type",2));
        String creditCardType = "";
        if(!ccType.equals("0") && !ccType.equals("-Select One-")) {
            creditCardType = ccType;
        }
        table.addCell(ModifyFontWithColspan(creditCardType,2));
        
        table.addCell(ModifyContentWithColspan("Name",2));
        table.addCell(ModifyFontWithColspan("",6));

        table.addCell(ModifyContentWithColspan("Number",2));
        table.addCell(ModifyFontWithColspan(String.valueOf(ccNum),2));
        
        table.addCell(ModifyContentWithColspan("Security Code",2));
        table.addCell(ModifyFontWithColspan(String.valueOf(ccSecCode),2));
        
        table.addCell(ModifyContentWithColspan("Expiry Date",2));
        table.addCell(ModifyFontWithColspan(ccExpDate,2));
        
        return table;
    }
    
    public static PdfPTable getConfirmedByTable(String confirmCaption, String name, String telNo, String faxNo, String email){
        PdfPTable table = new PdfPTable(8);
        
        table.addCell(ModifyContentWithColspan(confirmCaption,2));
        table.addCell(ModifyContentWithColspan("Telephone No.",2));
        table.addCell(ModifyContentWithColspan("Fax No.",2));
        table.addCell(ModifyContentWithColspan("Email",2));
        
        table.addCell(ModifyFontWithColspan(name,2));
        table.addCell(ModifyFontWithColspan(telNo,2));
        table.addCell(ModifyFontWithColspan(faxNo,2));
        table.addCell(ModifyFontWithColspan(email,2));
        
        return table;
    }
}
