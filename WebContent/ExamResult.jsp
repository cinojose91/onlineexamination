<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="Util.GenerateCertPDF" %>
<%@ page import="Entity.User" %>
<%@ page import="Method.UserController" %>

<%
int userid= (Integer) session.getAttribute("userId");

if(userid == 0){
	session.invalidate();
	response.sendRedirect("login.jsp");
	//return;
}

String score = request.getParameter("score");
if(score == null){
	score="0";
}

int examscorepercent =(int) Double.parseDouble(score);

String exampaperId= request.getParameter("exampaperId");
if(exampaperId==null){
	exampaperId="0";
}
int exampaperid= Integer.parseInt(exampaperId);

if(examscorepercent>=50){
	UserController userCtrl = new UserController();
	User user = userCtrl.getUserById(userid);
    GenerateCertPDF generateCertPdf = new GenerateCertPDF();
	generateCertPdf.createPdfForCert(user.getId(),user.getName(), user.getEmail(),exampaperid);
}


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exam Result</title>
</head>
<body>
<link href="include/css/bootstrap.min.css" rel="stylesheet">
<jsp:include page="navbar.jsp" />
<div class="page-header">
	<br>
	<h1 class="header text-center">Exam Result</h1>
</div>

<div align="center">
<br> <br>
<%if(examscorepercent>=50){%>
	<h3>Congratulation! You have passed your Exam.<br>Your certificate will be E-Mailed to you shortly.</h3>
	<br>
	<h3> Status: <b style="color:blue">Passed</b></h3>
	
<%}else{ %>
	<h3>I am sorry to informed you that you have  failed your exam!</h3>
	<h3> Status: <b style="color:red">Failed</b></h3>
<%} %>

<h3>Your score is: <strong><%=examscorepercent%> % </strong> </h3>

</div>
<jsp:include page="footer.jsp" />

</body>
</html>