<nav class="navbar navbar-default navbar-fixed-top">		
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Training Institution</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<% if(session.getAttribute("userId")!= null && Integer.parseInt(session.getAttribute("usertype").toString())==1 ) { %>
					<li><a href="ExamMaster.jsp">Exam Selection</a></li>
					<%  }else if(session.getAttribute("userId")!= null && Integer.parseInt(session.getAttribute("usertype").toString())==2 ) { %>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Lecture <span class="caret"></span></a>
						<ul class="dropdown-menu">

							<li role="separator" class="divider"></li>
							<li><a href="ExamPapers.jsp">Modules Management</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="ExamResults.jsp">Exam Results Statistics</a></li>
						</ul></li>
					<% } else { %>
					<li><a href="login.jsp#services">Services</a></li>
					<li><a href="login.jsp#whyus">Why Us</a></li>
					<li><a href="login.jsp#contact">Locate Us</a></li>
					<li><a href="login.jsp#page-header">Login</a></li>
					<li><a href="signup.jsp">Sign up</a></li>
					<% }%>

				</ul>
				<ul class="nav navbar-nav navbar-right">
					<% if(session.getAttribute("userId")!= null) { %>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Account <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="logout.jsp">Logout</a></li>
						</ul></li>
					<% } %>

				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>