
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="Method.ExamPaperController"%>
<%@ page import="Entity.ExamQuestion"%>
<%@ page import="Entity.ExamPaper"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Entity.StudentExamPapers"%>

<%
    int userId = (Integer) session.getAttribute("userId");
	if (userId == 0) {
		response.sendRedirect("login.jsp");
	}
	int userid = userId;

	String exampaperId = request.getParameter("exampaperId");
	if (exampaperId == null) {
		exampaperId = "0";

	}
	int exampaperid = Integer.parseInt(exampaperId);
%>
<%
	ArrayList<StudentExamPapers> list = new ArrayList<StudentExamPapers>();
	ExamPaperController ectrl = new ExamPaperController();
	list = ectrl.getStudentModuleList(userid);
%>
<html>
<head></head>
<link href="include/css/bootstrap.min.css" rel="stylesheet">
<link href="include/css/datatables.css" rel="stylesheet">
<jsp:include page="navbar.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exam Papers</title>
<script src="include/js/jquery-1.10.2.js"></script>
<script>
function fnOpenWindow(jsp) {
	window.open(jsp, 'Exam');
}
function fnHasStartedExam(userId,exampaperId) {
	//alert("fnHasStartedExam function is working!!" + userId + " and "  + exampaperId);
	$.ajax({
		url : 'HasExamStartedServlet?userId=' + userId
				+ '&exampaperId='+exampaperId,
		type : 'get',
		processData : false, // important
		contentType : false, // important
		success : function(result) {
			if (parseInt(result) == -1) {//not started
				fnOpenWindow('Exam.jsp?userId=' + userId
						+ '&exampaperId=' + exampaperId);
			} else if (parseInt(result) == 0) {//inprogress
				fnOpenWindow('Exam.jsp?userId=' + userId
						+ '&exampaperId=' + exampaperId);
			} else if (parseInt(result) == 1) {//ended
				alert('You have already attempted.');
			}

		}
	});
	
}
</script>

<body>
		<br>
		<br>
		<br>
		<br>
		<ol class="breadcrumb">
  	<li><a href="dashboard.jsp">Dashboard</a></li>
  	<li class="active">Exam Selection</li>
	</ol>
	<h1 class="header col-md-offset-1">
			Exam Selection</h1>
	<hr>
	<div class="row">
		<div class="col-md-10 col-lg-offset-1">
			<table class="table table-striped table-condensed row text-center" id="myTable" >
			<colgroup>
            <col class="col-md-1">
            <col class="col-md-3">
            <col class="col-md-4">
            <col class="col-md-2">
            <col class="col-md-2">
    		</colgroup>
				<thead>
					<tr>
						<td><strong>Exam #</strong></td>
						<td><strong>Exam Name</strong></td>
						<td><strong>Exam Description</strong></td>
						<td><strong>Exam Minutes</strong></td>
						<td><strong>Question #</strong></td>
						<td><strong>Score</strong></td>
					</tr>
				</thead>
				<tbody>
					<%
						for (StudentExamPapers ep : list) {
							System.out.println(ep);
					%>
					<tr>
						<td><a href="#" onclick="fnHasStartedExam(<%=userId%>,<%=ep.getId()%>)"class="btn btn-info btn-sm"><%=ep.getId()%></a></td>
						<td><%=ep.getName()%></td>
						<td><%=ep.getDescription()%></td>
						<td><%=ep.getExammins()%></td>
						<td><%=ep.getNoofquestions()%></td>
						<td><%if(ep.getStatus()!=-1){ 
							if(ep.getExamscore()>=50) {%>
						   		<span class="label label-success"><%=ep.getExamscore() %></span>
						   		<%} else { %>
						   		<span class="label label-danger"><%=ep.getExamscore() %></span>	
						   		<%} %>
						<%} else {%>
						    Not Attempted
						<%} %>
						</td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	<script src="include/js/datatables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#myTable').DataTable();
		});
	</script>
	
	</body>