<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="Method.ExamQuestionController" %>
    <%@ page import="Entity.ExamQuestion"%>
    <%@ page import="Entity.ExamAnswer"%>
    <%@ page import="java.util.ArrayList"%>
    <%! 
	ExamQuestionController examqustionCtrl = new ExamQuestionController();
    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">



<%
  String callJS ="";
  String id= request.getParameter("id");
	if(id == null){
		id = "0";	
	}
	int examquestionid = Integer.parseInt(id);
	
	String exampaperid= request.getParameter("exampaperid");
	if(exampaperid == null){
		exampaperid = "1";	
	}
	
	ExamQuestion eq = examqustionCtrl.getExamQuestionById(examquestionid);
	ArrayList<ExamAnswer> eaList = eq.getExamAnswerList();
	if(eaList == null){
		eaList = new ArrayList<ExamAnswer>();
		
	}
	String action = request.getParameter("action");
	if(action == null){
		action = "";
	}
	if(action.equals("update")){
		callJS="alert('Question Updated');opener.fnRefresh();window.close();";
	}
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exam Question</title>
</head>
<link href="include/css/bootstrap.min.css" rel="stylesheet">
<jsp:include page="navbar.jsp" />
<body>
<script src="include/js/jquery-1.10.2.js"></script>
<script>
	function validateCorrectCheck(obj){
		if(obj.checked == true){
			clearAllCheck();
			obj.checked=true;
		}
	}
	function clearAllCheck(){
		for(var i=0;document.getElementById('chkCorrectAns'+i) != null; i++){
			document.getElementById('chkCorrectAns'+i).checked= false;
			
		}	
	
	}
	function fnAddPossibleAnswer(){
		document.getElementById('jaction').value='add';
		document.frmexampaperquestion.submit();
		
	}
	
	function fnDeleteAns(id) {
		var vconfirm = confirm('Do you want to delete this answer?');
		if(vconfirm){
			document.frmexampaperquestion.action='DeleteServlet?action=deleteans&id=<%=id%>&exampaperid=<%=exampaperid%>&examansid='+id;
			document.frmexampaperquestion.method='post';
			document.frmexampaperquestion.submit();
		}
	}

</script>
		<br>
		<br>
		<br>
		<br>
	<ol class="breadcrumb">
  	<li><a href="dashboard.jsp">Dashboard</a></li>
  	<li><a href="ExamPapers.jsp">Modules Management</a></li>
  	<li><a href="ExamPaperMaster.jsp">Exam Paper Master</a></li>
  	<li class="active">Edit Exam Paper Question</li>
	</ol>
<div class="container">
   
   <div class="col-md-10 col-lg-offset-1">
  	<h1 class ="text-center">
		Edit Exam Paper Question
	</h1>
<form id="frmexampaperquestion" name="frmexampaperquestion" method="post" action="ExamQuestionServlet">
<table class="table table-striped table-condensed">
		<tr>
		<td>Question</td>
		<td colspan="2"><input type="text" size="80" id="txtQuestion" name="txtQuestion" value="<%=eq.getQuestion() %>" />
			<input type="hidden" id="examquestionid" name="examquestionid" value="<%=id%>" />
			<input type="hidden" id="exampaperid" name="exampaperid" value=<%=exampaperid%>>
		</td>
		
		</tr>
		<tr>
		
		<td>Score</td>
	    <td><input type="text" id="score" name="score" value="<%=eq.getScore()%>" size="80" ></td>

		</tr>
		<tr>
		<td>Add Possible Answer:</td>
	    <td colspan="2"><input type="text" id="txtAnswer" name="txtAnswer" value="" size="80">&nbsp;&nbsp;&nbsp;
		<input type="submit" value="Add" class="btn btn-info btn-xs"/></td>

		</tr>
		
		
		<%for(int i=0;i<eaList.size();i++){ 
			
			ExamAnswer ea;
			if(eaList.size()< i || eaList.size()==0 ){
				ea = new ExamAnswer();
				
			}else{
				ea = eaList.get(i);
				
			}
		%>
		<tr>
		<td>Possible Answer <%=i+1%></td>
		<td><input type="text" size="80" id="txtAnswer<%=i%>" name="txtAnswer<%=i%>" value="<%=ea.getAnswer()%>" />&nbsp;&nbsp;&nbsp;
			<input type="checkbox" id="chkCorrectAns<%=i%>" name="chkCorrectAns<%=i%>" value="1" onchange="validateCorrectCheck(this)"  <%if(ea.getIsCorrect()==1){%>Checked<%} %> > Correct Answer</input>
			<input type="hidden" id="answerId<%=i%>" name="answerId<%=i%>" value="<%=ea.getId()%>">
		
		</td>
		<td><input type="button" class='btn btn-danger btn-xs' value='Delete Answer' onclick="fnDeleteAns('<%=ea.getId()%>')" /></td>
		</tr>
		<%}%>
		
		<tr>
		<td colspan="3"><input type="submit" value="Update" class="btn btn-info"></td>
		</tr>
	
		
	</table>
</form>	
</div>
</div>
</div>
</body>
<jsp:include page="footer.jsp" />
<script>
<%=callJS%>
</script>
</html>