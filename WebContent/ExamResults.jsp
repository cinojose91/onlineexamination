<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="Method.ExamPaperController"%>
<%@ page import="Entity.ExamQuestion"%>
<%@ page import="Entity.ExamPaper"%>
<%@ page import="Entity.ExamPercentage"%>
<%@ page import="java.util.ArrayList"%>
<html>
<head></head>

<link href="include/css/bootstrap.min.css" rel="stylesheet">
<link href="include/css/datatables.css" rel="stylesheet">
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<jsp:include page="navbar.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exam Results</title>
<%
	ArrayList<ExamPaper> list = new ArrayList<ExamPaper>();
	ExamPaperController ectrl = new ExamPaperController();
	list = ectrl.getAvailablePapers();
%>
<body>
	<br>
		<br>
		<br>
		<br>
		<ol class="breadcrumb">
		<li><a href="dashboard.jsp">Dashboard</a></li>
  		<li class="active">Exam Results Statistics</li>
		</ol>
	<h1 class="header">Exam Results Statistics</h1>
	<hr>

	<div class="row">
		<div class="col-md-12">

			<div class="panel-group" id="accordion" role="tablist"
				aria-multiselectable="true">
				<%
					for (ExamPaper ep : list) {
				%>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading<%=ep.getId()%>">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion"
								href="#collapse<%=ep.getId()%>"
								aria-expanded=<%if(ep.getId()== 1){ %>"true"<%  } else{ %>"false"<%} %>
								aria-controls="collapse<%=ep.getId()%>"> <%=ep.getName()%>
							</a>
						</h4>
					</div>
					<div id="collapse<%=ep.getId()%>"
						class="panel-collapse collapse in" role="tabpanel"
						aria-labelledby="heading<%=ep.getId()%>">
						<div class="panel-body" >
							<div id="myfirstchart<%=ep.getId() %>" style="height: 250px;"></div>
						</div>
					</div>
				</div>
				<%
					}
				%>
			</div>

		</div>

	</div>
	<div id="myfirstchart" style="height: 250px;"></div>




	<jsp:include page="footer.jsp" />

	<script
		src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script
		src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
	<script type="text/javascript">
	<% for (ExamPaper ep : list) { 
		ExamPercentage eper = ectrl.getPercentage(ep.getId());
		if(eper.getPass()==0 && eper.getFail()==0){
			//continue;
		}
	%>
	
	Morris.Donut({
		  element: 'myfirstchart'+<%=ep.getId() %>,
		  data: [
		    {label: "Pass %", value: <%=eper.getPass()%>},
		    {label: "fail %", value: <%=eper.getFail()%>}
		  ],
		  colors: [
		           '#39B580',
		           '#FF0000'
		         ],
		});
	<% } %>

</script>