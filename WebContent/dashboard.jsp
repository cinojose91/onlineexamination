<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="Method.UserController"%>

<%! UserController userCtrl = new UserController(); %>
<%
	int userid = (Integer) session.getAttribute("userId");
    String name = (String) session.getAttribute("username");

	if (userid == 0) {
		session.invalidate();
		response.sendRedirect("login.jsp");
		//return;
	}
	
	ArrayList<Integer> userAccessRights = userCtrl.getUserAccessByUserid(userid);
	boolean isAdmin= false;
	boolean isLecturer=false;
	boolean isStudent= false;

	for(int i=0;i<userAccessRights.size();i++){
		int usertypeid =userAccessRights.get(i);
		if(usertypeid==1){
			isStudent=true;
		}else if(usertypeid==2){
			isLecturer=true;
		}else if(usertypeid==3){
			isAdmin=true;
		}
		
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<link href="include/css/bootstrap.min.css" rel="stylesheet">
</head>
<jsp:include page="navbar.jsp" />
		<br>
		<br>
		<br>
		<br>
		<ol class="breadcrumb">
  <li class="active">Dashboard</li>
</ol>
<div class="page-header" >
	<h1 class="col-md-10 col-md-offset-1">
	Dashboard <small class="col-md-offset-3">Welcome <%=name%> !</small>
	</h1>
</div>
<script src="include/js/jquery-1.10.2.js"></script>
<script>
		function fnOpenWindow(jsp) {
			if(jsp == 'ExamMaster.jsp'){
				var userId = document.getElementById('userId').value;
				var exampaperId = document.getElementById('exampaperId').value;
				//alert("ExamMaster is calling is working!!" + userId + " and "  + exampaperId);
				window.open(jsp + '?userId=' + userId + '&exampaperId='+ exampaperId, 'Exam');				
			}			
			else window.open(jsp, 'Exam');
		}
		function fnHasStartedExam() {
			var userId = document.getElementById('userId').value;
			var exampaperId = document.getElementById('exampaperId').value;
			$.ajax({
				url : 'HasExamStartedServlet?userId=' + userId
						+ '&exampaperId='+exampaperId,
				type : 'get',
				processData : false, // important
				contentType : false, // important
				success : function(result) {
					if (parseInt(result) == -1) {//not started
						fnOpenWindow('Exam.jsp?userId=' + userId
								+ '&exampaperId=' + exampaperId);
					} else if (parseInt(result) == 0) {//inprogress
						fnOpenWindow('Exam.jsp?userId=' + userId
								+ '&exampaperId=' + exampaperId);
					} else if (parseInt(result) == 1) {//ended
						alert('You have already attempted.');
					}

				}
			});
		}
		
	</script>
<form id='frmDashboard'>
	<input type="hidden" id="userId" name="userId" value="<%=userid%>" />
	<input type="hidden" id="exampaperId" name="exampaperId" value="2" />
	<div class="col-md-10 col-md-offset-1">
	<br>
	<table class="table table-striped table-condensed table-bordered">
	<%if(isAdmin){ // for admin view%>
		<tr>
			<td>For Admin</td>
		</tr>
		<tr>
			<td>Manage User</td>
		</tr>
	<%}%>
     <%if(isLecturer){ // for lecturer view %>
		<tr>
			<td>For Lecturer</td>
		</tr>
		<tr>
			<td><a href="#" onclick="fnOpenWindow('ExamPapers.jsp')">Modules Management</a></td>
		</tr>
	 <%}%>
	 <%if(isStudent){ // for Student view %>
		<tr>
			<td>For Student</td>
		</tr>
		<tr>
			<td><a href="#" onclick="fnOpenWindow('ExamMaster.jsp')">Exam Selection</a> </td>
		</tr>
	  <%}%>
	</table>
</form>
<jsp:include page="footer.jsp" />