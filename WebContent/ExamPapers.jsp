<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="Method.ExamPaperController"%>
<%@ page import="Entity.ExamQuestion"%>
<%@ page import="Entity.ExamPaper"%>
<%@ page import="java.util.ArrayList"%>
<html>
<head></head>

<link href="include/css/bootstrap.min.css" rel="stylesheet">
<link href="include/css/datatables.css" rel="stylesheet">
<jsp:include page="navbar.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exam Papers</title>
<%
	ArrayList<ExamPaper> list = new ArrayList<ExamPaper>();
	ExamPaperController ectrl = new ExamPaperController();
	list = ectrl.getAvailablePapers();
%>

<script>
function fnDeletePaper(id) {
	var vconfirm = confirm('Do you want to delete this Paper?');
	if(vconfirm){
		document.frmExampapers.action='DeleteServlet?action=deletepaper&exampaperid='+id;
		document.frmExampapers.method='post';
		document.frmExampapers.submit();
	}
}
</script>
<body>
<form id="frmExampapers" name="frmExampapers"></form>
		<br>
		<br>
		<br>
		<br>
		<ol class="breadcrumb">
  	<li><a href="dashboard.jsp">Dashboard</a></li>
  	<li class="active">Modules Management</li>
	</ol>
	<h1 class="header col-md-offset-1">
		Modules Management
		<button type="button" class="btn btn-default" aria-label="Left Align"
			data-toggle="modal" data-target="#myModal">
			<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
			Add
		</button>

	</h1>
	<hr>
	
	<div class="row">
		<div class="col-md-10 col-lg-offset-1">
			<table class="table table-striped table-condensed row text-center" id="myTable" >
			<colgroup>
            <col class="col-md-1">
            <col class="col-md-3">
            <col class="col-md-4">
            <col class="col-md-2">
            <col class="col-md-2">
            <col class="col-md-2">
    		</colgroup>
				<thead>
					<tr>
						<td><strong>Exam #</strong></td>
						<td><strong>Exam Name</strong></td>
						<td><strong>Exam Description</strong></td>
						<td><strong>Exam Minutes</strong></td>
						<td><strong>Question #</strong></td>
						<td><strong>Delete Paper</strong></td>
					</tr>
				</thead>
				<tbody>
					<%
						for (ExamPaper ep : list) {
					%>
					<tr>
						<td><a href="ExamPaperMaster.jsp?id=<%=ep.getId()%>" class="btn btn-success btn-sm"><%=ep.getId()%></a></td>
						<td><%=ep.getName()%></td>
						<td><%=ep.getDescription()%></td>
						<td><%=ep.getExammins()%></td>
						<td><%=ep.getNoofquestions()%></td>
						<td><a href="#" onclick="fnDeletePaper('<%=ep.getId()%>')" class="btn btn-danger">Delete Paper</a></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	<script src="include/js/datatables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#myTable').DataTable();
		});
	</script>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">New Exam</h4>
				</div>
				<div class="modal-body">
					<form data-parsley-validate method="POST" action="ExamPaperServlet">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Paper Name :</label> <input
										type="text" class="form-control" id="exampleInputEmail1"
										placeholder="Paper Name" name="txtPaperName" data-parsley-trigger="change" required>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Description</label> <input
										type="text" class="form-control"
										id="exampleInputPassword1" placeholder="Description" name="txtPaperDescription">
								</div>
								<div class="form-group">
									<label for="exampleInputFile">Paper Dur(Mins)</label>
								    <input
										type="text" class="form-control"
										id="exampleInputPassword1" placeholder="Duration Mins" name="txtdurationMin" data-parsley-trigger="change" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Semester :</label> <input
										type="text" class="form-control" id="exampleInputEmail1"
										placeholder="Semester" name="txtPaperSemester"   data-parsley-trigger="change" data-parsley-type="number" data-parsley-required>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">No of Question(s) :</label> <input
										type="text" class="form-control" id="exampleInputEmail1"
										placeholder="No of questions" name="txtnoofquestions" data-parsley-trigger="change" data-parsley-type="number" data-parsley-required>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Paper Dur(Hrs) :</label> <input
										type="text" class="form-control" id="exampleInputEmail1"
										placeholder="Duration Hours" name="txtdurationHr" data-parsley-trigger="change" data-parsley-type="number" data-parsley-required>
								</div>
								<input type="hidden" name="action" value="a">
								
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Add Exam</button>
						</div>
						</form>
				</div>
			</div>
		</div>
		</div></body>