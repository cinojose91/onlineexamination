<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="Method.ExamPaperController"%>
<%@ page import="Entity.ExamQuestion"%>
<%@ page import="Entity.ExamPaper"%>
<%@ page import="java.util.ArrayList"%>
<%! 
	ExamPaperController examPaperCtrl = new ExamPaperController();
%>
<%
	String callJs= "";
	String id= request.getParameter("id");
	if(id==null){
		id="1";
	}
	int exampaperid= Integer.parseInt(id);
	
	String action= request.getParameter("action");
	if(action==null){
		action="";
	}
	if(action.equals("update")){
		callJs="alert('Exam Paper Updated')";
	}

	ExamPaper ep = examPaperCtrl.getPaperById(exampaperid);
	ArrayList<ExamQuestion> eqlist = ep.getEqlist();
%>

<html>
<head></head>

<link href="include/css/bootstrap.min.css" rel="stylesheet">
<jsp:include page="navbar.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exam Paper Master</title>
<body>
<script>
function fnOpenWindow(id){
	window.open('ExamQuestion.jsp?exampaperid=<%=exampaperid%>&id='+id, 'ExamQuestion');	
}

function fnRefresh(){
	document.frmExamPaper.action="ExamPaperMaster.jsp";
	document.frmExamPaper.method="post";
	document.frmExamPaper.submit();
	
}

function fnDeleteQns(id) {
	var vconfirm = confirm('Do you want to delete this question?');
	if(vconfirm){
		document.frmExamPaper.action='DeleteServlet?action=deleteqns&id=<%=id%>&examqnsid='+id;
		document.frmExamPaper.method='post';
		document.frmExamPaper.submit();
	}
}

</script>
	<br>
	<br>
	<br>
	<br>
	<ol class="breadcrumb">
  	<li><a href="dashboard.jsp">Dashboard</a></li>
  	<li><a href="ExamPapers.jsp">Modules Management</a></li>
  	<li class="active">Exam Paper Master</li>
	</ol>
<div class="container">
  <div class="col-md-10 col-lg-offset-1">
	<h1>
		Exam Paper Master
	</h1>
	 <div class="row text-center">
		<form id="frmExamPaper" name="frmExamPaper" action="ExamPaperServlet" method="post">
		<table  class="table table-striped">
		<tr>
			<td>Paper name</td>
			<td><input type="text" id="txtPaperName" name="txtPaperName" value="<%=ep.getName()%>" /></td>
			<td>Paper Description</td>
			<td><input type="text" id="txtPaperDescription" name="txtPaperDescription" value="<%=ep.getDescription() %>" /></td>
			<td>Paper Year</td>
			<td><input type="text" id="txtPaperYear" name="txtPaperYear" value="<%=ep.getYear() %>" /></td>
		</tr>
		<tr>
			<td>Paper Semester</td>
			<td><input type="text" id="txtPaperSemester" name="txtPaperSemester" value="<%=ep.getSemester() %>" /></td>
			<td>Paper Duration (HR)</td>
			<td><input type="text" id="txtdurationHr" name="txtdurationHr" value="<%=ep.getExamhours( )%>" /></td>
			<td>Paper Duration (MIN)</td>
			<td><input type="text" id="txtdurationMin" name="txtdurationMin" value="<%=ep.getExammins() %>" /></td>
		</tr>
		<tr>
		   <td>No Of Questions</td>
		   <td><input type="text" id="txtnoofquestions" name="txtnoofquestions" value="<%=ep.getNoofquestions()%>" /></td>
		   <td> </td>
		   <td> </td>
		   <td> </td>
		   <td> </td>
		</tr>
		<tr>
			<td><a href="#" onclick="fnOpenWindow('0')" class="btn btn-info">[Add Question]</a>
				<input type="hidden" id="id" name="id" value="<%=exampaperid%>">		
			</td>
		</tr>
		
	</table>
	<table class="table table-striped table-condensed">
	<colgroup>
            <col class="col-md-1">
            <col class="col-md-9">
            <col class="col-md-2">
            <col class="col-md-2">
    </colgroup>
	<tr>
		<td><strong>Question Number</strong></td>
		<td><strong>Question</strong></td>
		<td><strong>Edit Question</strong></td>
		<td><strong>Delete Question</strong></td>
	</tr>
	<%for(int i=0;i<eqlist.size();i++){
		ExamQuestion eq= eqlist.get(i); 
		%>
	<tr>
		<td><%=i+1%></td>
		<td><%=eq.getQuestion()%></td>
		<td><a href="#" onclick="fnOpenWindow('<%=eq.getId()%>')" class="btn btn-success btn-xs">Edit Question</a></td>
		<td><a href="#" onclick="fnDeleteQns('<%=eq.getId()%>')" class="btn btn-danger btn-xs">[Delete Question]</a></td>
	</tr>
	<%} %>
	<tr>
		<td><input type="submit" value="[Update]" class="btn btn-info"></td>
	</tr>
	</table>
</form>
</div>
</div>
</div>
</body>
<jsp:include page="footer.jsp" />
<script type="text/javascript">

<%=callJs%>
$('#clock').countdown("2020/10/10", function(event) {
 var totalHours = event.offset.totalDays * 24 + event.offset.hours;
 $(this).html(event.strftime(totalHours + ' hr %M min %S sec'));
 });
</script>
