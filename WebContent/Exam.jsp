<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@	page import="Method.ExamController"%>
<%@	page import="Method.ExamPaperController"%>
<%@	page import="Entity.StudentExam"%>
<%@	page import="Entity.ExamQuestion"%>
<%@	page import="Entity.ExamAnswer"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Entity.ExamPaper" %>
<%@ page import="java.util.Date"%>

<%@include file="loading.jsp"%>
<%!ExamController examCtrl = new ExamController();
   ExamPaperController examPaperCtrl = new ExamPaperController();%>

<%
	String userId = request.getParameter("userId");
	if (userId == null) {
		userId = "0";
		response.sendRedirect("login.jsp");
	}
	int userid = Integer.parseInt(userId);

	String exampaperId = request.getParameter("exampaperId");
	if (exampaperId == null) {
		exampaperId = "0";

	}
	int exampaperid = Integer.parseInt(exampaperId);

	StudentExam studentexam = examCtrl.getStudentExamQuestionByUseridExamPaperId(userid, exampaperid);
	ArrayList<ExamQuestion> examquestionList = studentexam.getExamquestionlist();
	ExamPaper ep = examPaperCtrl.getPaperById(exampaperid);
	int duration = ep.getExammins();
	Long millseconds = Long.valueOf(duration) * 60000;
	
	ArrayList<ExamAnswer> list = new ArrayList<ExamAnswer>();
	list = examCtrl.getStudentAnswer(studentexam.getId());
	System.out.println(list);
%>

<%
	ArrayList<ExamPaper> list2 = new ArrayList<ExamPaper>();
	ExamPaperController ectrl = new ExamPaperController();
	list2 = ectrl.getAvailablePapers();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exam</title>
<link href="include/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="include/css/jquery-ui.css">
</head>

<jsp:include page="navbar.jsp" />


<!-- jquery tab -->
<!-- autosave function -->
<style>
div.savestatus {
	/* Style for the "Saving Form Contents" DIV that is shown at the top of the form */
	width: 200px;
	padding: 2px 5px;
	border: 1px solid gray;
	background: #fff6e5;
	-webkit-box-shadow: 0 0 8px #818181;
	box-shadow: 0 0 8px #818181;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	color: red;
	position: absolute;
	top: -10px;
}

form#feedbackform div { /*CSS used by demo form*/
	margin-bottom: 9px;
}
</style>

<script>
function fnOpenWindow(jsp) {
	window.open(jsp, 'Exam');
}
function fnHasStartedExam(userId,exampaperId) {
	//alert("fnHasStartedExam function is working!!" + userId + " and "  + exampaperId);
	$.ajax({
		url : 'HasExamStartedServlet?userId=' + userId
				+ '&exampaperId='+exampaperId,
		type : 'get',
		processData : false, // important
		contentType : false, // important
		success : function(result) {
			if (parseInt(result) == -1) {//not started
				fnOpenWindow('Exam.jsp?userId=' + userId
						+ '&exampaperId=' + exampaperId);
			} else if (parseInt(result) == 0) {//inprogress
				fnOpenWindow('Exam.jsp?userId=' + userId
						+ '&exampaperId=' + exampaperId);
			} else if (parseInt(result) == 1) {//ended
				alert('You have already attempted.');
			}

		}
	});
	
}
</script>

<body>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>

<h1 class="header col-md-offset-1">
	Exam</h1>

	
<form id="frmExam" name="frmExam" method="post" action="ExamServlet">
	<div class="col-md-2 col-sm-offset-1">
			<table class="table table-condensed row text-left" id="myTable" >
		<tr>
			<td><%=studentexam.getStarttime()%> 
			    <input type="hidden" id="txtId" name="txtId" value="<%=studentexam.getId()%>" /> 
				<input type="hidden" id="txtExamQuestionsList" name="txtExamQuestionsList" value="<%=studentexam.getStrquestionslist()%>" />
				<input type="hidden" id="exampaperId" name="exampaperId" value="<%=exampaperid%>"/> 
				</td>
		</tr>
	</table>
</div>

	<div class="row">
		<!-- Main Row -->
		<div class="col-md-9 col-sm-offset-1">

			<div class="tabbable">
				<ul class="nav nav-pills nav-stacked col-md-2">
					<%
						for (int i = 0; i < examquestionList.size(); i++) {
					%>
					<li <%if (i == 0) {%> class="active" <%}%>><a
						href="#tabs-<%=i + 1%>" data-toggle="tab">Question <%=i + 1%>
							<!-- <span class="glyphicon glyphicon-ok pull-right"
							aria-hidden="true"></span>--></a></li>
					<%
						}
					%>
				</ul>
				<div class="tab-content col-md-10 well ">
					<%
						for (int i = 0; i < examquestionList.size(); i++) {
							ExamQuestion examquestion = examquestionList.get(i);
							ArrayList<ExamAnswer> answerList = examquestion.getExamAnswerList();
					%>
					<div class="tab-pane<%if (i == 0) {%> active<%}%>"
						id="tabs-<%=i + 1%>">
						<h2><%=examquestion.getQuestion()%>
							<input type="hidden" id="txtExamQuestionId<%=i%>"
								name="txtExamQuestionId<%=i%>" value="<%=examquestion.getId()%>">
							<input type="hidden" id="txtExamQuestionScore<%=i%>"
								name="txtExamQuestionScore<%=i%>"
								value="<%=examquestion.getScore()%>">
						</h2>


						<%
							int myans = examCtrl.getMyanswerId(list, examquestion.getId());
							for (int j = 0; j < answerList.size(); j++) {
									ExamAnswer possibleAns = answerList.get(j);
								    
						%>
						<div class="radio">
							<label><input type="radio" name="rdAns<%=i%>"
								
								value="<%=possibleAns.getId()%>" <% if(myans == possibleAns.getId() ) {%>checked="checked"<% } %> /><%=possibleAns.getAnswer()%></label>
						</div>
						<%
							}
						%>

					</div>
					<%
						}
					%>
				</div>
			</div>

		</div>
		<div class="col-md-2" >
		<h4>Time Left :<div id="clock" style="color:blue"></h4></div>
		<div id="autosave"></div></div>
	</div>


	<br>
	<tr>
		<td><input type="submit" id="btnsubmit" value="Submit"
			class="btn btn-success col-md-offset-2" /></td>
	</tr>
</form>

<jsp:include page="footer.jsp" />
<script > 
var fiveSeconds = new Date().getTime() + <%=millseconds %>;
var isSubmit=false;
$('#clock').countdown(fiveSeconds, {elapse: true} )
 .on('update.countdown', function(event) {
	 if (event.elapsed) { // Either true or false
		 	if(!isSubmit){
		 		isSubmit= true;
		 		$( "#btnsubmit" ).click();
		 	}
	    } else {
	    	 $(this).html(event.strftime('%H:%M:%S'));
	    }
 	
 });
</script>
<script src="include/js/jquery-1.10.2.js"></script>
<script src="include/js/jquery-ui.js"></script>

<!-- jquery tab -->
<script>
	$(function() {
		$("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
		$("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");

		$("#overlay").hide();
		$("#dvLoading").hide();
		
		$("#btnsubmit").click(function() {
			$("#overlay").show();
			$("#dvLoading").show();
		});

	});
	//script to save the form.
	$('input:radio').click(function() {
		$.ajax({
			url : "ExamAutoSave",
			method: "POST",
			data:  $("#frmExam").serialize(), 
			beforeSend : function(xhr) {
				$("#autosave").html("saving form....");
			}
		}).done(function(data) {
			$("#autosave").html("saving completed");
			//var today = new Date();
			//var dateString = today.toLocaleFormat("%d-%b-%Y");
			<% Date dt = new Date(); %>
			$("#autosave").html("Last saved <%=dt %>");
			if (console && console.log) {
				console.log("Sample of data:", data.slice(0, 100));
			}
		});
	});
	function updatetick(){
		
	}
	window.Parsley.on('form:success', function() {
		// This global callback will be called for any field that fails validation.
		$("#btnsubmit").click(function() {
			$("#overlay").show();
			$("#dvLoading").show();
		});
	});
	window.Parsley.on('field:error', function() {
		// This global callback will be called for any field that fails validation.
		console.log('Validation failed for: ', this.$element);
	});
</script>
<script src="include/js/autosaveform.js"></script>
<script>
	var formsave1 = new autosaveform({
		formid : 'frmExam',
		pause : 1000
	//<--no comma following last option!
	})
</script>

<!--Script for countdown timer-->


</body>
</html>