<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="include/css/bootstrap.min.css" rel="stylesheet">
<link href="include/css/stylish-portfolio.css" rel="stylesheet">
<link href="include/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<jsp:include page="navbar.jsp" />
<div class="page-header bg-primary col-md-6 col-md-offset-3 text-center">`
  <h1>Sign Up </h1>

<form class="form-horizontal" action="SignUp" method="POST" data-parsley-validate>
  <div class="form-group">
    <label for="inputEmail3" class="col-md-3 control-label">Full Name:</label>
    <div class="col-sm-8">
      <input type="text"  name="fname" class="form-control" id="inputname" placeholder="Name" data-parsley-trigger="change" required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">User ID:</label>
    <div class="col-sm-8">
      <input type="text" name ="userid" class="form-control" id="inputuserid" placeholder="User Id" data-parsley-trigger="change" required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Password:</label>
    <div class="col-sm-8">
      <input type="password" name ="password" class="form-control" id="inputPassword" placeholder="Password" data-parsley-trigger="keyup" data-parsley-minlength="7" required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Confirm Password:</label>
    <div class="col-sm-8">
      <input type="password" name ="cpassword" class="form-control" id="inputpasswordc" placeholder="Confirm Password" data-parsley-equalto="#inputPassword" required>
    </div>
  </div>
   <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Email:</label>
    <div class="col-sm-8">
      <input type="email" name ="email" class="form-control" id="inputemail" placeholder="Email" data-parsley-trigger="change" required >
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-8">
    <br>
      <button type="submit" class="btn btn-success">Sign Up</button>
      <button class="btn btn-danger" type="reset">Reset</button>
    </div>
  </div>
</form>
</div>
<section>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h4><strong>Training Institution Pte Ltd</strong>
                    </h4>
                    <p>50 Nanyang Ave<br>Singapore 639798</p>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-phone fa-fw"></i>+65 6791 1744</li>
                        <li><i class="fa fa-envelope-o fa-fw"></i>  <a href="mailto:">inquiry@Training_Institution.com</a>
                        </li>
                    </ul>
                    <br>
                    <ul class="list-inline">
                        <li>
                            <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
                        </li>
                    </ul>
                    <hr class="small">
                    <p class="text-muted">Copyright &copy; Training_Institution.com 2015</p>
                </div>
            </div>
        </div>
    </section>
<jsp:include page="footer.jsp" />
