<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	String result = (String) session.getAttribute("userresult");
	if (result == null) {
		result = "";
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login page</title>
<link href="include/css/bootstrap.min.css" rel="stylesheet">
<link href="include/css/stylish-portfolio.css" rel="stylesheet">
<link href="include/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<jsp:include page="navbar.jsp" />
	<header class="header">
        <div class="text-vertical-center">
            <h1>Training Institution</h1>
            <h3>Convenience &amp; Accessible</h3>
            <br>
            <a href="login.jsp#services" class="btn btn-dark btn-lg">Find Out More</a>
        </div>
    </header>
    <section id="services" class="services bg-primary">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2>Online Examination Services</h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-desktop fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Online Examination</strong>
                                </h4>
                                <a href="#login.jsp#whyus" class="btn btn-light">Learn More</a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-globe fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Recognition</strong>
                                </h4>
                                <a href="login.jsp#whyus" class="btn btn-light">Learn More</a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-map-marker fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Locate Us</strong>
                                </h4>
                                <a href="login.jsp#contact" class="btn btn-light">Learn More</a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-sign-in fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Member Login</strong>
                                </h4>
                                <a href="login.jsp#page-header" class="btn btn-light">Learn More</a>
                            </div>
                        </div>
                     </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <section id="whyus" class="services bg-info">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2>Why Us</h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-graduation-cap fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Success Rate</strong>
                                </h4>
                                <h3><strong>90%</strong></h3>
                             </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-users fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Repeat Learners</strong>
                                </h4>
                                <h3><strong>35%</strong></h3>
                           </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-map-marker fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Student Engagement</strong>
                                </h4>
                                <h3><strong>100%</strong></h3>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-sign-in fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Certified Courses</strong>
                                </h4>
                                <h3><strong>65%</strong></h3>
                            </div>
                        </div>
                     </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
     <section id="contact" class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2820.44620270327!2d103.68195472594768!3d1.3485335985045344!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da0f0a99014463%3A0xb8bb0800c52d8219!2sNanyang+Technological+University!5e0!3m2!1sen!2ssg!4v1444633189757" width="100%" height="100%" frameborder="0" style="pointer-events:none;" allowfullscreen></iframe>
        <br />
        <small>
            <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7136295905807!2d103.68094601497579!3d1.3483152619554113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da0f0a99014463%3A0xb8bb0800c52d8219!2sNanyang+Technological+University!5e0!3m2!1sen!2ssg!4v1444494119835"></a>
        </small>
        </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h4><strong>Training Institution Pte Ltd</strong>
                    </h4>
                    <p>50 Nanyang Ave<br>Singapore 639798</p>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-phone fa-fw"></i>+65 6791 1744</li>
                        <li><i class="fa fa-envelope-o fa-fw"></i>  <a href="mailto:">inquiry@Training_Institution.com</a>
                        </li>
                    </ul>
                    <br>
                    <ul class="list-inline">
                        <li>
                            <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
                        </li>
                    </ul>
                    <hr class="small">
                    <p class="text-muted">Copyright &copy; Training_Institution.com 2015</p>
                </div>
            </div>
        </div>
    </section>
<section id = page-header>
	<div class="page-header bg-primary col-md-4 col-md-offset-4 text-center">
		<h1>Login</h1>
			<div class="col-md-10">
				<form action="LoginServlet" method="post" class="form-horizontal"
				data-parsley-validate>
		<%
			if (result.equals("invalid")) {
		%>
			<div class="alert alert-danger" role="alert">Invalid User!</div>
		<%
			}
		%>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">User Name: </label>
			<div class="col-sm-8">
				<input type="text" name="txtusername" class="form-control"
					id="txtusername" placeholder="username"
					data-parsley-trigger="change" required>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">Password: </label>
			<div class="col-sm-8">
				<input type="password" name="txtpassword" class="form-control"
					id="txtpassword" placeholder="Password"
					data-parsley-trigger="change" required>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success">Login</button>
				<button class="btn btn-danger" type="reset">Reset</button>
			</div>
			
		</div>
	</form>
</div>
</div>
</section>
<div class="col-md-4 col-md-offset-4">
<a href="signup.jsp" class="btn btn-warning"> Sign Up Now!</a>
</div>
<jsp:include page="footer.jsp" />
<script>
 // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
</script>
</body>
</html>